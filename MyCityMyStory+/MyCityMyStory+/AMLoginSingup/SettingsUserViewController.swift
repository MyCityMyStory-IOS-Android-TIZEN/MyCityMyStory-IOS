//
//  SettingsUserViewController.swift
//  MyCityMyStory+
//
//  Created by ESPRIT on 07/12/2016.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SettingsUserViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBOutlet weak var pwdReset: AMInputView!
    
    func resetPassword(email : String){
        
        FIRAuth.auth()?.sendPasswordReset(withEmail: email, completion: { (error) in
            if error == nil {
                let alertController = UIAlertController(title: "Error !", message: "Check your Inbox Mail , to confirm your REST Password", preferredStyle: .alert)
                
                let Action = UIAlertAction(title: "Save",
                                           style: .default) { action in}
                alertController.addAction(Action)
                self.present(alertController, animated: true, completion: nil)
            }else {
               
                
                let alertController = UIAlertController(title: "Error !", message: "Check your Mail ^^' ", preferredStyle: .alert)
                
                let Action = UIAlertAction(title: "Save",
                                           style: .default) { action in}
                alertController.addAction(Action)
                self.present(alertController, animated: true, completion: nil)
                
                
            }
        })
    }
 
    @IBAction func presetPwd(_ sender: Any) {
        resetPassword(email: self.pwdReset.textFieldView.text!)
        dismiss(animated: true, completion: nil)

    }
  
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
  
     

}
