//
//  Venue.swift
//  MyCityMyStory
//
//  Created by Chebbi on 11/2/16.
//  Copyright © 2016 Chebbi. All rights reserved.
//

import UIKit

class Venue: NSObject {
    
    var  city:String!
    var   country:String!
    var   fullAddress : String!
    var   latitude:Double!
    var   longitude:Double!
    var   state:String!
    var   street:String!
    
    init( Lat : Double , Long : Double){
        latitude = Lat
        longitude = Long
    }
    
    init (dic :    [String : Any] ) {
        
        city =  dic["city"] as! String!
        country =  dic["country"] as! String!
        fullAddress =  dic["fullAddress"] as! String!
        latitude =  dic["latitude"] as! Double!
        longitude =  dic["longitude"] as! Double!
        state =  dic["state"] as! String!
        street =  dic["street"] as! String!
     }
    
}
