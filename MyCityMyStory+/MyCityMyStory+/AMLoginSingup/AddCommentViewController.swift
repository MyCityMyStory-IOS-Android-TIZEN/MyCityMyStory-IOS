//
//  AddCommentViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Chebbi on 11/9/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import Firebase
import Foundation

class AddCommentViewController: UIViewController,UITextViewDelegate {
    
    let rootref = FIRDatabase.database().reference()
    

     @IBOutlet weak var sweetTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        sweetTextView.layer.borderColor = UIColor.black.cgColor
        sweetTextView.layer.borderWidth = 0.5
        sweetTextView.layer.cornerRadius = 5
        sweetTextView.delegate = self
        
        sweetTextView.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
 

    @IBAction func AddCommentAction(_ sender: Any) {
        
       let comments = rootref.child("Comments").childByAutoId()
        let corp : String = sweetTextView.text
        let dateComment = ""
    
        
        let user : Int = 1
        let event : Int = 1
        let ObjetComments : [String : Any] =
            [ 
              "corp" : corp  ,
              "user" : user ,
              "event" : 0
            , "eventIdFb" : 093434
            , "eventIdAllEvent" : 0
            , "dateTimeComment" : "02/04/2016"
            ,"nbJaime" : 0 ]
        
        
        comments.setValue(ObjetComments)
    
        dismiss(animated: true,
                completion: nil)
         
        
    }
  
    
    
   
}
