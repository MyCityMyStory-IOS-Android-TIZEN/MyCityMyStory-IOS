//
//  DemoTableViewController.swift
//  TestCollectionView
//
//  Created by Alex K. on 24/05/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import expanding_collection
import Social
import FBSDKCoreKit
import SafariServices       // for SFSafariViewController

//        self.navigationController?.popToRootViewController(animated: true)

class DemoTableViewController: ExpandingTableViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var locationEvent: UILabel!
    var event : Event!
  var s = ""
    @IBOutlet weak var map: MKMapView!
  fileprivate var scrollOffsetY: CGFloat = 0
    
    
    
    @IBOutlet var dateTimeStart: UILabel!
    
    @IBOutlet var dateTimeEnd: UILabel!
    
    @IBOutlet var DescriptionEvent: UITextView!
    
    @IBAction func safariEvent(_ sender: Any) {
        print(self.event.eventUrl)
        let svc = SFSafariViewController(url: URL(string: self.event.shareUrl)!)
        svc.delegate = self
        self.present(svc, animated: true, completion: nil)


    }
    func getDescription( fbId : String) {
        let parameters = ["fields": "description"]
        //application access token
        let parame = "646925218810245|3J_39mIglVtqF81kZYIFZ9k4_y0"
        
        let graphRequest = FBSDKGraphRequest(graphPath:  fbId, parameters: parameters,tokenString: parame,version: "v2.8",httpMethod:"GET")
        
        graphRequest?.start { connection, result, error in
            // If something went wrong, we're logged out
            if (error != nil) {
                //ignore error
                
                return
            }
            if let result = result as? [String: Any] {
                
                if let  ev : String = (result["description"] as! String){
                    print("description ",ev)
                   self.DescriptionEvent.text = ev
                }
                
                
            }
        }
        
    }
    @IBAction func openWithSafariVC(_ sender: AnyObject)
    {
       
    }
    
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        //self.navigationController?.popToRootViewController(animated: true)

          controller.dismiss(animated: true, completion: nil)
        
    }

    
    
    @IBAction func shareFB(_ sender: Any) {
        
        shareToFacebook()
    }
    
     func shareToFacebook(){
        var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        
        
      
        
        let thumbnailURL = URL(string: event.thumbUrlLarge)

        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
               shareToFacebook.add(image)
            })
        }
        
        
       shareToFacebook.add(URL(string: event.shareUrl))
        
        self.present(shareToFacebook, animated: true, completion: nil)
        
    }

  override func viewDidLoad() {
    super.viewDidLoad()
    //configureNavBar()
    let image1 = UIImage.Asset.BackgroundImage.image
    tableView.backgroundView = UIImageView(image: image1)
    
    /**********/
    let pin = MKPointAnnotation()
    pin.coordinate.latitude = event.venue.latitude
    pin.coordinate.longitude =  event.venue.longitude
    pin.title = event.eventName
    
    map.addAnnotation(pin)
    
    let latDelta:CLLocationDegrees = 0.1
    
    let lonDelta:CLLocationDegrees = 0.1
    
    let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
    let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(event.venue.latitude, event.venue.longitude)
    
    let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
    
    map.setRegion(region, animated: true)
    
    
    /**********/

    
    locationEvent.text = event.location
    
    /******/
    dateTimeStart.text = event.startTimeDisplay
    dateTimeEnd.text = event.endTimeDisplay

    /*******/
    
    /*********/
    //getAllUsersRatingEvent()
    /*********/
     getDescription(fbId: event.eventIdFb)
    
  }
    
    /*func  getAllUsersRatingEvent(){
        let rootref = FIRDatabase.database().reference()
        rootref.child("StarRating").observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot.value)
            
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                   print("SNAP USER : ",snap.value)
                }
             //   self.tableView.reloadData()
                
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        

    }*/
    
}
// MARK: Helpers

extension DemoTableViewController {
  
  fileprivate func configureNavBar() {
    navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    navigationItem.rightBarButtonItem?.image = navigationItem.rightBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
  }
}

// MARK: Actions

extension DemoTableViewController {
  
  @IBAction func backButtonHandler(_ sender: AnyObject) {
    // buttonAnimation
    let viewControllers: [DemoViewController?] = navigationController?.viewControllers.map { $0 as? DemoViewController } ?? []

    
    
    for viewController in viewControllers {
      if let rightButton = viewController?.navigationItem.rightBarButtonItem as? AnimatingBarButton {
        rightButton.animationSelected(false)
      }
    }
    popTransitionAnimation()
  }
}

// MARK: UIScrollViewDelegate

extension DemoTableViewController {
  
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y < -25 {
      // buttonAnimation
      let viewControllers: [DemoViewController?] = navigationController?.viewControllers.map { $0 as? DemoViewController } ?? []

      for viewController in viewControllers {
        if let rightButton = viewController?.navigationItem.rightBarButtonItem as? AnimatingBarButton {
          rightButton.animationSelected(false)
        }
      }
      popTransitionAnimation()
    }
    scrollOffsetY = scrollView.contentOffset.y
  }
}
 
