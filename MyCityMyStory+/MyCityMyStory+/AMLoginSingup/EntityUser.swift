//
//  User.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/17/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import Foundation

import Firebase

import UIKit

class EntityUser {
      static let sharedInstance = EntityUser()
    var  Firstname : String = ""
    var Lastname : String = ""
    var birthday: String = ""
    var city :String = ""
    var country :String = ""
    var phone : String = ""
    var sexe:String = ""
    var urlImage:String = ""
    var image: UIImage?
    var moreInformation = false
   
    init(){
        
    }
    
    init(Firstname: String, Lastname: String, city: String ,country :String,sexe :String,phone : String,birthday : String,urlImage : String) {
        self.Lastname = Lastname
        self.Firstname = Firstname
        self.birthday = birthday
        // self.addedByUser = addedByUser"phone": ""+phone,
        self.city = city
        self.country = country
        self.sexe = sexe
        self.phone=phone
        self.urlImage = urlImage
    }
    init(Firstname: String, Lastname: String, city: String ,country :String,sexe :String,phone : String,birthday : String ) {
        self.Lastname = Lastname
        self.Firstname = Firstname
        self.birthday = birthday
        // self.addedByUser = addedByUser"phone": ""+phone,
        self.city = city
        self.country = country
        self.sexe = sexe
        self.phone=phone
     }
    
    
    init( snap :FIRDataSnapshot) {
        
        Lastname = snap.childSnapshot(forPath: "lastname").value as! String
        Firstname = snap.childSnapshot(forPath: "firstname").value as! String
        country = snap.childSnapshot(forPath: "country").value as! String
        city = snap.childSnapshot(forPath: "city").value as! String
        phone = snap.childSnapshot(forPath: "phone").value as! String
        sexe = snap.childSnapshot(forPath: "sexe").value as! String
        birthday = snap.childSnapshot(forPath: "birthday").value as! String
        urlImage = snap.childSnapshot(forPath: "urlImageUser").value as! String
    }
    
    
    func toAnyObject() -> Any {
        print("lastname"+Lastname)
        
        return [
            "lastname": ""+Lastname,
            "firstname": ""+Firstname ,
            "country": ""+country,
            "city" : ""+city,
            "phone": ""+phone,
            "sexe": ""+sexe,
            "birthday": ""+birthday,
            "urlImageUser": urlImage
            
            
        ]
    }
}
