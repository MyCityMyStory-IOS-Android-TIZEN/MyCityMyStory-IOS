 

//
//  DetailEventSearchNearMeViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Chebbi on 11/18/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import Alamofire
import UIKit
 import FBSDKCoreKit

class DetailEventCalendar: MIPivotPage {
    
    
    @IBOutlet var descriptionTxt: UITextView!
    var  event : Event!
    var tempp : Double!
    var mainTemp : [String: AnyObject]!
    
    @IBOutlet weak var imageEvent: UIImageView!
    
    
    
    
    @IBOutlet weak var DateTimeStart: UILabel!
    
    @IBOutlet weak var DateTimeEnd: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
        
        pivotPageController.menuView.clipsToBounds = true

            }
    
    func getDescription( fbId : String) {
        let parameters = ["fields": "description"]
        //application access token
        let parame = "646925218810245|3J_39mIglVtqF81kZYIFZ9k4_y0"
        
        let graphRequest = FBSDKGraphRequest(graphPath:  fbId, parameters: parameters,tokenString: parame,version: "v2.8",httpMethod:"GET")
        
        graphRequest?.start { connection, result, error in
            // If something went wrong, we're logged out
            if (error != nil) {
                //ignore error
                
                return
            }
            if let result = result as? [String: Any] {
                
                if let  ev : String = (result["description"] as! String){
                    print("description ",ev)
                    self.descriptionTxt.text = ev
                }
                
                
            }
        }
        
    }

    override func shouldShowPivotMenu() -> Bool { return false }
    override func pivotPageShouldHandleNavigation() -> Bool { return false }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /***********Date Time start & end***********/
        DateTimeStart.text=event.startTimeDisplay
        DateTimeEnd.text=event.endTimeDisplay
        /********************************************/
        
        /*********Location************/
         /*****************************/
        
        
        
        
        //  lblTemperature.text = event.temperature as! String
        /**********/
        
        /***********/
        
        /********ImageEvent********/
        let thumbnailURL = URL(string:  event.thumbUrlLarge)
        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
                self.imageEvent.image = image
            })
        }
        
        /**************************/
        
         getDescription(fbId: event.eventIdFb)
        // Do any additional setup after loading the view.
    }
    /*********************/
    
   
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
