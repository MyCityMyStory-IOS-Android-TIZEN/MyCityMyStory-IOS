//
//  InviteFriendsViewController.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/3/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Social

class InviteFriendsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func shareToFacebook(){
        var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        shareToFacebook.setInitialText("https://www.facebook.com/javaDevelopmentSolutions")
        self.present(shareToFacebook, animated: true, completion: nil)
        
    }

}
