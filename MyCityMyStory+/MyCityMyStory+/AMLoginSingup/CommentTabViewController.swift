//
//  ViewController.swift
//  MyCityMyStory
//
//  Created by Chebbi on 11/1/16.
//  Copyright © 2016 Chebbi. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import Foundation

class CommentTabViewController: UIViewController  , UITableViewDelegate,UITableViewDataSource {
    
    
    
   // @IBOutlet weak var snipper: UIActivityIndicatorView!
    let rootref = FIRDatabase.database().reference()

    var CommentsArray : [Comments] = [Comments] ()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

      //  snipper.startAnimating()
        
      //  UIApplication.shared.beginIgnoringInteractionEvents()
        tableView.delegate = self
        tableView.dataSource = self
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        CommentsArray =  [Comments] ()
         getAllComments()
        
            }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CommentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell")!
        
        let lblUserName:UITextView = cell.viewWithTag(101) as! UITextView
        lblUserName.text = CommentsArray[indexPath.row].corp
        
        
        
        
        
        
        return cell
    }
    
   /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "SegueDetailEvent", sender: tab[indexPath.row])
    } */
    
  /*  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : DetailViewController = segue.destination as! DetailViewController
        svc.event = sender as! Event
    }*/
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
              rootref.child("Comments").child(self.CommentsArray[indexPath.row].id).removeValue()
            
            CommentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
          
        }
    }
    /*********************/
    func getTempBy(Latitude lat : Double , longitude lon : Double){
        
    }
    
    /****`
     *****************/
    func getAllComments()   {
   
        
        rootref.child("Comments").observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot.value)
            
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    self.CommentsArray.append(Comments(snap: snap))
                }
                self.tableView.reloadData()
                
            }
        }) { (error) in
            print(error.localizedDescription)
        }

        
        
    }
    
    
}

