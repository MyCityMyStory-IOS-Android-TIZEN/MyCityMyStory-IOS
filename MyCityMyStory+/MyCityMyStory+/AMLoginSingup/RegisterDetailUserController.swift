 
  //
 //  UserDetailViewController.swift
 //  MyCityMyStoryVersion1.1
 //
 //  Created by Syrine Dridi on 11/17/16.
 //  Copyright © 2016 Developers Academy. All rights reserved.
 //
 
 import UIKit
 import FirebaseAuth
 import  FirebaseDatabase
 import MobileCoreServices
 import FirebaseAuth
 import Firebase
 
 class RegisterDetailUserController: UIViewController ,SCPopDatePickerDelegate , UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    let datePicker = SCPopDatePicker()
    let date = Date()
    var takePick : Bool = false
    var imagePicker: UIImagePickerController!
       let storage = FIRStorage.storage()
    @IBOutlet var cameraPhoto: UIImageView!
    let rootRef = FIRDatabase.database().reference()
    static var isSelectedCountry = false

    
    
    @IBOutlet var tvFirstname: UITextField!
    
    @IBOutlet var tvSex: UISegmentedControl!
    
    @IBOutlet var tvBirthday: UILabel!
    
    @IBOutlet var tvCity: UILabel!
    
    @IBOutlet var tvCountry: UILabel!
    
    
    @IBOutlet var tvPhone: UITextField!
    
    
    let ref: FIRDatabaseReference? = nil
    
    @IBOutlet var tvLastName: UITextField!
    static var countrySelected = ""
    static var citySelected = ""
    static var countrySelectedIndex = 0
    static var citySelectedIndex = 0
    var urlImageUser : String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // for hitting return
        tvPhone.delegate = self
        tvFirstname.delegate = self
        tvLastName.delegate = self
        //****
        // for tapping
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterDetailUserController.dismissKeyboard)))
        
        
        
        
                self.tvCity.text = ""
                self.tvFirstname.text = ""
                self.tvLastName.text = ""
                self.tvCountry.text = ""
                self.tvPhone.text = ""
                let s =  "Man"
                
                if s == "Woman" {
                    self.tvSex.selectedSegmentIndex = 1
                }else{
                    self.tvSex.selectedSegmentIndex = 0
                }
                self.tvBirthday.text = ""
                self.urlImageUser =  ""
         
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    // for tapping
    func dismissKeyboard() {
        tvPhone.resignFirstResponder()
        tvFirstname.resignFirstResponder()
        tvLastName.resignFirstResponder()
    }
    
    // for hitting return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tvPhone.resignFirstResponder()
        tvFirstname.resignFirstResponder()
        tvLastName.resignFirstResponder()
        
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tvCountry.text = RegisterDetailUserController.countrySelected
        tvCity.text = RegisterDetailUserController.citySelected
        
        
    }
    @IBAction func changeDateBirthday(_ sender: Any) {
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.date
        self.datePicker.showBlur = true
        self.datePicker.datePickerStartDate = self.date
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = UIColor.darkGray
        self.datePicker.showCornerRadius = false
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)
    }
    
    
    func scPopDatePickerDidSelectDate(_ date: Date) {
        tvBirthday.text = date.stringFromFormat("MM'/'dd'/'yyyy'")
    }
    
    @IBAction func btnAdd(_ sender: AnyObject) {
        
        if takePick == true {
            if tvFirstname.text != "" && tvLastName.text != "" && tvPhone.text != ""
                && tvCity.text != "" && tvCountry.text != "" && tvBirthday.text != "" {
                
                
                
                UploadImage()
                
            }else  {
                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                    print("INDEX : \(index)")
                    
                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                   strMessage: "Erreur you should check all field is not empty",
                   btnArray: ["Yup I get It"], strTitle: "Erreur")
            }
            
        }else{
            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                print("INDEX : \(index)")
                
            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
               strMessage: "take picture now , or select from your galerie  ^^",
               btnArray: ["Yup I get It"], strTitle: "Erreur")
        }
       
        
        
        
        
        
    }
    
   
    @IBAction func selectCountry(_ sender: Any) {
         RegisterDetailUserController.isSelectedCountry = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController!
        RegisterDetailUserController.citySelected = ""
        self.present(vc!, animated: true, completion: nil)
    }
    
    
    
    @IBAction func selectCity(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CityViewController") as! CityViewController!
        
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func gelerieAction(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        
          imagePicker.sourceType = .photoLibrary
         
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        takePick = true
        present(imagePicker,animated:  true , completion:  nil)
    }
   
    @IBAction func takePhoto(_ sender: Any) {
        imagePicker =  UIImagePickerController()
         imagePicker.sourceType = .camera
         
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
  takePick = true
        present(imagePicker,animated:  true , completion:  nil)
  
        
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image =   info[UIImagePickerControllerEditedImage] as? UIImage{
            cameraPhoto.image = image
            
        }else{
            
            print("there was a problem getting the image")
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func UploadImage(){
        // Data in memory
        var data = NSData()
        data = UIImageJPEGRepresentation(cameraPhoto.image!, 0.8)! as NSData
        /***********************/
          let stringFromDate = Date().iso8601
        if let dateFromString = stringFromDate.dateFromISO8601 {
            print("timeeeeeee",dateFromString.iso8601)      // "2016-06-18T05:18:27.935Z"
        }
         // "2016-06-18T05:18:27.935Z"
        /************************/
        // Create a reference to the file you want to upload
        let imageRef = storage.reference().child("images_users").child(stringFromDate)
        let metaData = FIRStorageMetadata()
        metaData.contentType = "image/jpg"
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = imageRef.put(data as Data, metadata: metaData){(metaData,error) in
            if let error = error {
                print(error.localizedDescription)
                // Uh-oh, an error occurred!
            } else {
                // Metadata contains file metadata such as size, content-type, and download URL.
                let downloadURL = metaData!.downloadURL()!.absoluteString
                let firstname = self.tvFirstname.text
                let lastName = self.tvLastName.text
                var sex = "Man"
                if (self.tvSex.selectedSegmentIndex == 1){
                    sex = "Woman"
                }
                let phone = self.tvPhone.text
                let city = self.tvCity.text
                let country = self.tvCountry.text
                
                
                let dateString = self.tvBirthday.text
                
                // 2
                let userItem = EntityUser(Firstname: firstname!, Lastname: lastName!, city: city!
                    ,country :country!,sexe :sex,phone : phone!,birthday : dateString!, urlImage : downloadURL)       // 3
                let eventItemRef = self.rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!)
                
                // 4
                eventItemRef.setValue(userItem.toAnyObject())
                
                
                /*let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginMyCityMyStory") as! UINavigationController!
                self.show(nextViewController!, sender:nil)*/
                self.navigationController?.popToRootViewController(animated: true)
                
                
            }
            
            
            
            
        }
    }
 }
 
 extension Date {
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    var iso8601: String {
        return Date.iso8601Formatter.string(from: self)
    }
 }
 
 extension String {
    var dateFromISO8601: Date? {
        return Date.iso8601Formatter.date(from: self)
    }
 }
