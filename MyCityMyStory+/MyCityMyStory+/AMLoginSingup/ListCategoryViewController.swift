//
//  ListCategoryViewController.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/5/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

class ListCategoryViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!

    
    let imgIcons  : [String] = ["Business","Concerts","Exhibitions","Festivale","Meetups","Music","Parties"]
    let  imgBackGrounds : [String] = ["BusinessImg","ConcertsImg","ExhibitionsImg","FestivaleImg","MeetupsImg","MusicImg","PartiesImg"]
    
    let lblTextDescriptions : [String]  = ["Events - Conferences for Startups, Entrepreneurs & Enterprises",
                                "Concerts Events, Schedule, Line Ups & Tickets",
                                "Trade shows, Expos & Art Galleries"
        ," "," "," ",
         "Best Night Clubs, Parties & Pub Crawls"]
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgBackGrounds.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCellCategorySearch")!
   
        
        let imgBackGround: UIImageView = cell.viewWithTag(101) as! UIImageView
        
        imgBackGround.image = UIImage(named: imgBackGrounds[indexPath.row])
       
        let imgIcon: UIImageView = cell.viewWithTag(102) as! UIImageView
        
        imgIcon.image = UIImage(named: imgIcons[indexPath.row])
        
        let lblTitre: UILabel = cell.viewWithTag(103) as! UILabel
        
        lblTitre.text = imgIcons[indexPath.row]
        
        let lblTextDescription : UITextView = cell.viewWithTag(104) as! UITextView

        lblTextDescription.text = lblTextDescriptions[indexPath.row]
       
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     performSegue(withIdentifier: "segueSearchEventByCategory", sender: imgIcons[indexPath.row])
     }
    
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let svc : SearchEventAPIByCategoryViewController = segue.destination as! SearchEventAPIByCategoryViewController
     svc.category = sender as! IndexPath?
     }
 



}
