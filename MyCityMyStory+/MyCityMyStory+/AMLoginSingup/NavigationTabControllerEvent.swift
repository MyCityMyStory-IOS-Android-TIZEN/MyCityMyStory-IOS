//
//  NavigationControllerEventAPI.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 11/24/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

class NavigationTabControllerEvent: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

extension NavigationTabControllerEvent: MIPivotRootPage {
    
    func imageForPivotPage() -> UIImage? {
        return UIImage(named: "iconB")
    }
    func rootPivotPageWillHide() {
        print("From NavigationTabControllerEvent rootPivotPageWillHide")
        self.dismiss(animated: true, completion: {})

    }
    func rootPivotPageDidShow() {
        print("From NavigationTabControllerEvent rootPivotPageDidShow")
        
    }
    
    
}
