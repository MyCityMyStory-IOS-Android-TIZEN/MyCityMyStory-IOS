//
//  ViewController.swift
//  AMLoginSingup
//
//  Created by amir on 10/11/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FBSDKLoginKit
import FBSDKCoreKit
import NVActivityIndicatorView
import Firebase

enum AMLoginSignupViewMode {
    case login
    case signup
}

class LoginViewController: UIViewController ,FBSDKLoginButtonDelegate , NVActivityIndicatorViewable{
    

    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    
    let animationDuration = 0.25
    var mode:AMLoginSignupViewMode = .signup
    
    
    @IBOutlet weak var btnFacebook: FBSDKLoginButton!
    
    //MARK: - background image constraints
    @IBOutlet weak var backImageLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var backImageBottomConstraint: NSLayoutConstraint!
    
    
    //MARK: - login views and constrains
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginContentView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginButtonVerticalCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginWidthConstraint: NSLayoutConstraint!
    
    
    //MARK: - signup views and constrains
    @IBOutlet weak var signupView: UIView!
    @IBOutlet weak var signupContentView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var signupButtonVerticalCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupButtonTopConstraint: NSLayoutConstraint!
    
    
    //MARK: - logo and constrains
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoButtomInSingupConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoCenterConstraint: NSLayoutConstraint!
   
    
    @IBOutlet weak var forgotPassTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var socialsView: UIView!
    
    
    
    //MARK: - controller
    override func viewDidLoad() {
        super.viewDidLoad()
        // set view to login mode
        toggleViewMode(animated: false)
        
        //add keyboard notification
         NotificationCenter.default.addObserver(self, selector: #selector(keyboarFrameChange(notification:)), name: .UIKeyboardWillChangeFrame, object: nil)
     //   configureFacebook()
        
        /**FB Login***/
        
        /*
        
        FIRAuth.auth()!.addStateDidChangeListener() { auth, user in
            // 2
            if user != nil {
                print (user?.email)
                
                print("viewDidLoad    Login user != nil ")
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageMyCityMyStory") as UIViewController!
                self.present(nextViewController!, animated:true, completion:nil)
                print("user connected")
            }
            else {
                
                if (FBSDKAccessToken.current() == nil)
                {
                    
                    print("not logged in ")
                }
                else
                {
                    print("logged in already")
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageMyCityMyStory") as UIViewController!
                    self.present(nextViewController!, animated:true, completion:nil)
                    print("user connected")
                    
                    
                }
                
                
                print("user error")
            }
        }
        if (FBSDKAccessToken.current() == nil)
        {
            
            print("not logged in ")
        }
        else
        {
            print("logged in already")
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageMyCityMyStory") as UIViewController!
            self.present(nextViewController!, animated:true, completion:nil)
            print("user connected")
            
            
        }*/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    
    /************Traitement du Login***************/
    @IBOutlet weak var txtPassword: AMInputView!
    
    @IBOutlet weak var txtEmail: AMInputView!
    
    
    let rootref = FIRDatabase.database().reference()
    
    //MARK: - button actions
    @IBAction func loginButtonTouchUpInside(_ sender: AnyObject) {
   
        if mode == .signup {
             toggleViewMode(animated: true)
        }else if mode == .login {
            
            
            if (txtEmail.textFieldView.text != ""  && txtPassword.textFieldView.text != "" ){
                
                
                /************Start Activity`************************/
                
                
                
                let size = CGSize(width: 30, height:30)
                
                self.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 5)!)
                self.perform(#selector(self.delayedStopActivity),
                             with: nil,
                             afterDelay: 2.5)
                /**************************************************/
                
                FIRAuth.auth()!.signIn(withEmail: txtEmail.textFieldView.text!,
                                       password: txtPassword.textFieldView.text!) {
                    auth, user in
                             
                        
          if auth != nil {
                        /**********************Save Token of users ********************/
                    let rootref = FIRDatabase.database().reference()
            
                        if let refreshedToken = FIRInstanceID.instanceID().token() {
                
                                print("InstanceID token: \(refreshedToken)")
                                let TOKENS = rootref.child("Tokens").child("\((FIRAuth.auth()?.currentUser?.uid)!)")
                
                
                                let ObjetComments : [String : Any] =
                                                    [
                                                            "Token" : refreshedToken
                                                    ]
                
                
                            TOKENS.setValue(ObjetComments)
                
                
                        }
                   /**********************End Token of users ********************/
            
            
                       let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageMyCityMyStory") as UIViewController!
                        self.show(nextViewController!, sender:nil)
                          }
                         else {
                           let alert = UIAlertController(title: "Mot de passe ghalit", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                          }

                }

              /*  FIRAuth.auth()!.addStateDidChangeListener() { auth, user in
                 }  */
            }
            else {
                
                let alertController = UIAlertController(title: "Error !", message: "Please check you email and Password should not be empty", preferredStyle: .alert)
                
                let Action = UIAlertAction(title: "Save",
                                           style: .default) { action in}
                alertController.addAction(Action)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            /************END Activity`************************/
            /**************************************************/
            

            
            
            
        }
        
        
        
    }
    /***********Register***********/
    
    @IBOutlet weak var TvPassword: AMInputView!
    @IBOutlet weak var TvEmail: AMInputView!
    @IBAction func signupButtonTouchUpInside(_ sender: AnyObject) {
   
        if mode == .login {
            toggleViewMode(animated: true)
        }else if mode == .signup {
            
            if TvEmail.textFieldView.text != "" && TvPassword.textFieldView.text != "" {
                
                /************Start Activity`************************/
                
                
                
                let size = CGSize(width: 30, height:30)
                
                self.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 5)!)
                self.perform(#selector(self.delayedStopActivity),
                             with: nil,
                             afterDelay: 2.5)
                /**************************************************/

                
                FIRAuth.auth()!.createUser(withEmail: TvEmail.textFieldView.text!,
                                           password: TvPassword.textFieldView.text!)
                 { user, error in
                    if error == nil {
                        print("user created " )
                        
                        
                       
                        let firstname = "Not Yet"
                        let lastName = "Not Yet"
                        let sex = "Not Yet"
                        
                        let phone = "52192130"
                        let city = "Not Yet"
                        let country = "Not Yet"
                        
                        let date = "02/27/1994"
                        // let dateFormatter = DateFormatter()
                        // dateFormatter.dateFormat = "MM/dd/yyyy"
                        // let dateString = dateFormatter.string(from: date)
                        
                        // 2
                        let userItem = EntityUser(Firstname: firstname, Lastname: lastName, city: city
                            ,country :country,sexe :sex,phone : phone,birthday : date)       // 3
                        let eventItemRef = self.rootref.child("users").child((FIRAuth.auth()?.currentUser?.uid)!)
                        
                        // 4
                        eventItemRef.setValue(userItem.toAnyObject())
                        
                        //    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "userDetailProfile") as! UserDetailViewController
                        
                        //    self.present(nextViewController, animated:true, completion:nil)
                        
                        print("user created 222" )
                        
                        let nextView = self.storyboard?.instantiateViewController(withIdentifier: "RegisterDetailUserController") as! RegisterDetailUserController!
                        
                      //  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterDetailNavigationController") as! UINavigationController!
                        
                    //    self.show(nextViewController!, sender: nil)
                        
                        self.navigationController!.pushViewController(nextView!, animated: true)
                    }
                    else {
                        let alertController = UIAlertController(title: "Error !", message: "Your email exist .. or there is problem", preferredStyle: .alert)
                        
                        let Action = UIAlertAction(title: "Save",
                                                   style: .default) { action in}
                        alertController.addAction(Action)
                        self.present(alertController, animated: true, completion: nil)

                    }
                }
                
            }else {
                let alertController = UIAlertController(title: "Error !", message: "you should all field not empty", preferredStyle: .alert)
                
                let Action = UIAlertAction(title: "Save",
                                           style: .default) { action in}
                alertController.addAction(Action)
                self.present(alertController, animated: true, completion: nil)
                
            }

            
            

        }
    }
    
    
    
    //MARK: - toggle view
    func toggleViewMode(animated:Bool){
    
        // toggle mode
        mode = mode == .login ? .signup:.login
        
        
        // set constraints changes
        backImageLeftConstraint.constant = mode == .login ? 0:-self.view.frame.size.width
        
        
        loginWidthConstraint.isActive = mode == .signup ? true:false
        logoCenterConstraint.constant = (mode == .login ? -1:1) * (loginWidthConstraint.multiplier * self.view.frame.size.width)/2
        loginButtonVerticalCenterConstraint.priority = mode == .login ? 300:900
        signupButtonVerticalCenterConstraint.priority = mode == .signup ? 300:900
        
        
        //animate
        self.view.endEditing(true)
        
        UIView.animate(withDuration:animated ? animationDuration:0) {
            
            //animate constraints
            self.view.layoutIfNeeded()
            
            //hide or show views
            self.loginContentView.alpha = self.mode == .login ? 1:0
            self.signupContentView.alpha = self.mode == .signup ? 1:0
            
            
            // rotate and scale login button
            let scaleLogin:CGFloat = self.mode == .login ? 1:0.4
            let rotateAngleLogin:CGFloat = self.mode == .login ? 0:CGFloat(-M_PI_2)
            
            var transformLogin = CGAffineTransform(scaleX: scaleLogin, y: scaleLogin)
            transformLogin = transformLogin.rotated(by: rotateAngleLogin)
            self.loginButton.transform = transformLogin
            
            
            // rotate and scale signup button
            let scaleSignup:CGFloat = self.mode == .signup ? 1:0.4
            let rotateAngleSignup:CGFloat = self.mode == .signup ? 0:CGFloat(-M_PI_2)
            
            var transformSignup = CGAffineTransform(scaleX: scaleSignup, y: scaleSignup)
            transformSignup = transformSignup.rotated(by: rotateAngleSignup)
            self.signupButton.transform = transformSignup
        }
        
    }
    
    
    //MARK: - keyboard
    func keyboarFrameChange(notification:NSNotification){
        
        let userInfo = notification.userInfo as! [String:AnyObject]
        
        // get top of keyboard in view
        let topOfKetboard = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue .origin.y
        
        
        // get animation curve for animate view like keyboard animation
        var animationDuration:TimeInterval = 0.25
        var animationCurve:UIViewAnimationCurve = .easeOut
        if let animDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
            animationDuration = animDuration.doubleValue
        }
        
        if let animCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
            animationCurve =  UIViewAnimationCurve.init(rawValue: animCurve.intValue)!
        }
        
        
        // check keyboard is showing
        let keyboardShow = topOfKetboard != self.view.frame.size.height
        
        
        //hide logo in little devices
        let hideLogo = self.view.frame.size.height < 667
        
        // set constraints
        backImageBottomConstraint.constant = self.view.frame.size.height - topOfKetboard
        
        logoTopConstraint.constant = keyboardShow ? (hideLogo ? 0:20):50
        logoHeightConstraint.constant = keyboardShow ? (hideLogo ? 0:40):60
        logoBottomConstraint.constant = keyboardShow ? 20:32
        logoButtomInSingupConstraint.constant = keyboardShow ? 20:32
        
        forgotPassTopConstraint.constant = keyboardShow ? 30:45
        
        loginButtonTopConstraint.constant = keyboardShow ? 25:30
        signupButtonTopConstraint.constant = keyboardShow ? 23:35
        
        loginButton.alpha = keyboardShow ? 1:0.7
        signupButton.alpha = keyboardShow ? 1:0.7
        
        
        
        // animate constraints changes
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(animationDuration)
        UIView.setAnimationCurve(animationCurve)
        
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
    }
    
    //MARK: - hide status bar in swift3
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
  
    
    @IBAction func LoginFireBase(_ sender: Any) {
      
    }
 
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
              
        
        if error != nil {
            // that's mean we have a problem
            print("Classe Login : WTF : ",error)
            return
        }
        print("succeffully logged in with facebook....")
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageMyCityMyStory") as UIViewController!
        self.present(nextViewController!, animated:true, completion:nil)
         
    }
    
     func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
 
    /*  func displayWalkthroughs()
     {
     
     // Create the walkthrough screens
     let userDefaults = UserDefaults.standard
     let displayedWalkthrough = userDefaults.bool(forKey: "DisplayedWalkthrough")
     if !displayedWalkthrough {
     
     if let pageViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewController") as? PageViewController {
     self.present(pageViewController, animated: true, completion: nil)
     
     
     
     }
     
     }
     }*/
    
 
    @IBAction func registerAction(_ sender: Any) {
        
          }
    
    func delayedStopActivity() {
        stopAnimating()
    }
    
    func configureFacebook()
    {
        btnFacebook.readPermissions = ["public_profile", "email", "user_friends"];
        btnFacebook.delegate = self
    }
 
  
 
}

