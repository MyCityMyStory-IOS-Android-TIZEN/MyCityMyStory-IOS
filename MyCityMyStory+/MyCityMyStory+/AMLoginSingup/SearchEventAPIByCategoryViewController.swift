
 
 //
 //  DemoViewController.swift
 //  TestCollectionView
 //
 //  Created by Alex K. on 12/05/16.
 //  Copyright © 2016 Alex K. All rights reserved.
 //
 
 import UIKit
 import Alamofire

 
 
 class SearchEventAPIByCategoryViewController: ExpandingViewController {
    
    var category : IndexPath? = nil
        let imgIcons  : [String] = ["Business","Concerts","Exhibitions","Festivale","Meetups","Music","Parties"]
    
    typealias ItemInfo = (imageName: String, title: String)
    fileprivate var cellsIsOpen = [Bool]()
    fileprivate var items: [Event] = []
    
    @IBAction func exiit(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
  
    
 }
 
 // MARK: life cicle
 
 extension SearchEventAPIByCategoryViewController {
    
    override func viewDidLoad() {
        itemSize = CGSize(width: 256, height: 290)
        super.viewDidLoad()
        
        
        getAllEvent(city: "Tunis", state: "TU", country: "Tunisia", category : imgIcons[(category?.row)!])
        
        
    }
    /*********************/
    func getAllEvent(city : String,state : String,country : String,category : String)   {
        // Do any additional setup after loading the view, typically from a nib.
        items=[]
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": "72848bd8222a46e8a7de8626e7c3f9fd"
        ]
        
        Alamofire.request("https://api.allevents.in/events/list/?city="+city+"&state="+state+"&country="+country+"&category="+category, method: .post ,headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value{
                    let jsonResult:Dictionary = json  as! [String: Any]
                    let data : NSArray = (jsonResult["data"] as! NSArray)
                    
                    for l in data  {
                        let ev  : Event = Event(dic: l as! [String:Any])
                        
                            self.items.append(ev)
                                                
                    }
                    print("size items = ",self.items.count)
                    
                    self.registerCell()
                    self.fillCellIsOpeenArry()
                    self.addGestureToView(self.collectionView!)
                    //  self.configureNavBar()
                    self.collectionView?.reloadData()
                    
                }
                
            case .failure(let error):
                print(error)
                //   print("In Function getAll Event "+String(self.tab.count))
                
            }
            }.resume()
        
        
        
    }
    
 }
 
 // MARK: Helpers
 
 extension SearchEventAPIByCategoryViewController {
    
    fileprivate func registerCell() {
        
        let nib = UINib(nibName: String(describing: DemoCollectionViewCell.self), bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: DemoCollectionViewCell.self))
    }
    
    fileprivate func fillCellIsOpeenArry() {
        for _ in items {
            cellsIsOpen.append(false)
        }
    }
    
    fileprivate func getViewController(index : IndexPath) -> ExpandingTableViewController {
        let storyboard = UIStoryboard(storyboard: .Main)
        
        let toViewController: SearchEventAPIByCategoryTableViewController = storyboard.instantiateViewController()
        toViewController.event = items[index.row]
        return toViewController
    }
    
    fileprivate func configureNavBar() {
        navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
 }
 
 /// MARK: Gesture
 
 extension SearchEventAPIByCategoryViewController {
    
    fileprivate func addGestureToView(_ toView: UIView) {
        let gesutereUp = Init(UISwipeGestureRecognizer(target: self, action: #selector(SearchEventAPIByCategoryViewController.swipeHandler(_:)))) {
            $0.direction = .up
        }
        
        let gesutereDown = Init(UISwipeGestureRecognizer(target: self, action: #selector(SearchEventAPIByCategoryViewController.swipeHandler(_:)))) {
            $0.direction = .down
        }
        toView.addGestureRecognizer(gesutereUp)
        toView.addGestureRecognizer(gesutereDown)
    }
    
    func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        let indexPath = IndexPath(row: currentIndex, section: 0)
        guard let cell  = collectionView?.cellForItem(at: indexPath) as? DemoCollectionViewCell else { return }
        // double swipe Up transition
        if cell.isOpened == true && sender.direction == .up {
            pushToViewController(getViewController(index : indexPath))
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(true)
            }
        }
        
        let open = sender.direction == .up ? true : false
        cell.cellIsOpen(open)
        cellsIsOpen[(indexPath as NSIndexPath).row] = cell.isOpened
    }
 }
 
 // MARK: UIScrollViewDelegate
 
 extension SearchEventAPIByCategoryViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       // pageLabel.text = "\(currentIndex+1)/\(items.count)"
    }
 }
 
 // MARK: UICollectionViewDataSource
 
 extension SearchEventAPIByCategoryViewController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        guard let cell = cell as? DemoCollectionViewCell else { return }
        
        let index = (indexPath as NSIndexPath).row % items.count
        let info = items[index]
        
        /***********/
        /***/
        let thumbnailURL = URL(string: items[indexPath.row].thumbUrlLarge)
        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
                cell.backgroundImageView?.image = image
            })
        }
        
        /***********/
        
        cell.customTitle.text = info.eventName
        cell.cellIsOpen(cellsIsOpen[index], animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DemoCollectionViewCell
            , currentIndex == (indexPath as NSIndexPath).row else { return }
        
        
        if cell.isOpened == false {
            cell.cellIsOpen(true)
            
        } else {
            pushToViewController(getViewController(index : indexPath))
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(true)
            }
        }
    }
    
    /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let svc : DemoTableViewController = segue.destination as! DemoTableViewController
     svc.event = sender as! Event
     }*/
    
 }
 
 // MARK: UICollectionViewDataSource
 extension SearchEventAPIByCategoryViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DemoCollectionViewCell.self), for: indexPath)
    }
    
 }
