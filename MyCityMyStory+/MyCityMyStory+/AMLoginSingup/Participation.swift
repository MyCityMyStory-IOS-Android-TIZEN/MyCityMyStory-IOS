//
//  Participation.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/17/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import Foundation
import  Firebase
class Participation: NSObject {
    
    var  event_id : String!
    var  user_id : String
    var  id : String!

    
    
    
    
    init(event_id: String, user_id: String, id: String ) {
        self.event_id = event_id
        self.user_id = user_id
        self.id = id

        
        
        
    }
    
    init( snap :FIRDataSnapshot) {
        
        event_id = snap.childSnapshot(forPath: "event_id").value as! String
        user_id = snap.childSnapshot(forPath: "user_id").value as! String
        id = snap.childSnapshot(forPath: "id").value as! String
        
    }
    
    
    func toAnyObject() -> Any {
        print("id"+id!)
        return [
            "event_id": ""+event_id,
            "user_id": ""+user_id ,
            "date_participation": Date().stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        
        ]
    }
    
}
