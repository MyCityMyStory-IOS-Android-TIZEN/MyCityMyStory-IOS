//
//  DetailViewController.swift
//  MyCityMyStory
//
//  Created by Chebbi on 11/3/16.
//  Copyright © 2016 Chebbi. All rights reserved.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {

    var  event : Event!
    var tempp : Double!
     var mainTemp : [String: AnyObject]!
   
    @IBOutlet weak var imageEvent: UIImageView!
    
    
    @IBOutlet weak var imageWeatherView: UIImageView!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    
    @IBOutlet weak var DateTimeStart: UILabel!
  
    @IBOutlet weak var DateTimeEnd: UILabel!
    
    @IBOutlet weak var lblTemperature: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        do{
            getTempBy(Latitude: event.venue.latitude,longitude: event.venue.longitude)
            
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /***********Date Time start & end***********/
        DateTimeStart.text=event.startTimeDisplay
         DateTimeEnd.text=event.endTimeDisplay
        /********************************************/
        
        /*********Location************/
         btnLocation.setTitle(event.location, for:UIControlState.normal)
        /*****************************/
        
        
        
        
      //  lblTemperature.text = event.temperature as! String
        /**********/
        
        /***********/
        
        /********ImageEvent********/
        let thumbnailURL = URL(string:  event.thumbUrlLarge)
        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
                self.imageEvent.image = image
            })
        }

        /**************************/
        

        // Do any additional setup after loading the view.
    }
    /*********************/
    
    func getIconWeather(){
        //http://openweathermap.org/img/w/10d.png
        
    }
    
    func getTempBy(Latitude lat : Double , longitude lon : Double) {
        
        
        
        
        /***Temp**/
        //http://api.openweathermap.org/data/2.5/find?lat=36.812519073486&lon=10.170880317688&cnt=1&appid=949b43b9ef9cbc4e772477f7cba8fd5e
        /****ICON **/
        //https://openweathermap.org/weather-conditions
        /******/
        
        Alamofire.request("http://api.openweathermap.org/data/2.5/find?lat=\(event.venue.latitude)&lon=\(event.venue.longitude)&cnt=1&appid=949b43b9ef9cbc4e772477f7cba8fd5e", method: .get ).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value{
                    let jsonResult:Dictionary = json  as! [String: Any]
                    let list : NSArray = (jsonResult["list"] as! NSArray)
                    let mainWeather : Dictionary = list[0] as! [String: Any]
                    
                    
                    
                   self.mainTemp   = mainWeather["main"] as! [String: AnyObject]
                  //  print("Ahmed \(self.mainTemp["temp"]! as! Double)")
                    
                   self.lblTemperature.text = "\(self.mainTemp["temp"]! as! Double)"
                    
                    let listWeather : NSArray = (mainWeather["weather"] as! NSArray)
                    let objWeather : Dictionary = listWeather[0] as! [String: Any]

                     /*********************/
                    /********IconWeather********/
                    let iconURL = URL(string: "http://openweathermap.org/img/w/\(objWeather["icon"]!).png"  )
                    let networkService = NetworkService(url: iconURL!)
                    networkService.downloadImage { (imageData) in
                        let image = UIImage(data: imageData as Data)
                        DispatchQueue.main.async(execute: {
                            self.imageWeatherView.image = image
                        })
                    }
                    
                    /**************************/

                    /*********************/
                    
                    
                   }
                
            case .failure(let error):
                print(error)
              }
            }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let svc : GoogleMapViewController = segue.destination as! GoogleMapViewController
            svc.event = event
        }
   
    }

   
}
