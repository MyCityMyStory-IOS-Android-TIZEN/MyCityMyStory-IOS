//
//  ViewController.swift
//  Rating Demo
//
//  Created by Glen Yi on 2014-09-05.
//  Copyright (c) 2014 On The Pursuit. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, FloatRatingViewDelegate {
    
     @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var liveLabel: UILabel!
    @IBOutlet var updatedLabel: UILabel!
    
    
    
    
    let user : String = "a64mn3aaOBZfjiQ5ugyu4TuVEdj2"
    var event : Event!
    
    let rootref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /** Note: With the exception of contentMode, all of these
            properties can be set directly in Interface builder **/
        
        // Required float rating view params
       self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.delegate = self
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 0
       
        self.floatRatingView.editable = true
        self.floatRatingView.halfRatings = false
        self.floatRatingView.floatRatings = false
        
        
        var childSring = "\(event.Id!)__\(user)"
        print(childSring)

        rootref.child("StarRating").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            
            if snapshot.hasChild(childSring){
                
                 self.floatRatingView.rating = snapshot.childSnapshot(forPath: childSring+"/nbStar").value as! Float
                
          }else{
                self.floatRatingView.rating = 0
            }
            
            
        })
        // Labels init
        self.liveLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        
        print("Loeaded")
 }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
        self.liveLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        
        
        
        let StarRatings = rootref.child("StarRating").child("\(event.Id!)__\(user)")
        
        
    
        let ObjetStarRating : [String : Any] =
            [
                
                "nbStar" : self.floatRatingView.rating ,
                "event" : event.Id,
                "user" : user
            ]
        
        
        StarRatings.setValue(ObjetStarRating)
        
        
        
        print("is Updating ")
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        
        print("did Update")
    }
    
    
}

