//
//  SendFeedBackViewController.swift
//  MyCityMyStory+
//
//  Created by ESPRIT on 07/12/2016.
//  Copyright © 2016 amirs.eu. All rights reserved.
//


 
    import UIKit        // UI: user interface
    import MessageUI
    
    class SendFeedBackViewController: MIPivotPage, MFMailComposeViewControllerDelegate {
        
        
        // declare MIME types (Multipurpose Internet Mail Extension)
        // it defines which kind of information to send via email
        
        fileprivate enum MIMEType: String {
            case jpg = "image/jpeg"
            case png = "image/png"
            case doc = "application/msword"
            case ppt = "application/vnd.ms-powerpoint"
            case html = "text/html"
            case pdf = "application/pdf"
            
            init?(type: String) {
                switch type.lowercased() {
                case "jpg": self = .jpg
                case "png": self = .png
                case "doc": self = .doc
                case "ppt": self = .ppt
                case "html": self = .html
                case "pdf": self = .pdf
                default: return nil
                }
            }
        }
        
        // MARK: - Share Quotes via Email
        
        func showMailComposerWith(_ attachmentName: String)
        {
            if MFMailComposeViewController.canSendMail() {
                let subject = "Great quote from Duc Tran at Developers Academy"
                let messageBody = "Hey , Duc at DA here. This is very motivational!"
                let toRecipients = ["Ahmed.chebbi@esprit.tn"]
                
                let mailComposer = MFMailComposeViewController()
                mailComposer.mailComposeDelegate = self
                mailComposer.setSubject(subject)
                mailComposer.setMessageBody(messageBody, isHTML: false)
                mailComposer.setToRecipients(toRecipients)
                
                let fileParts = attachmentName.components(separatedBy: ".")
                let fileName = fileParts[0]
                let fileExtension = fileParts[1]
                
                if let filePath = Bundle.main.path(forResource: fileName, ofType: fileExtension) {
                    if let fileData = try? Data(contentsOf: URL(fileURLWithPath: filePath)), let mimeType = MIMEType(type: fileExtension) {
                        mailComposer.addAttachmentData(fileData, mimeType: mimeType.rawValue, fileName: fileName)
                        
                        show(mailComposer, sender:  nil)
                    }
                }
            }
        }
        
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
        {
            switch result.rawValue
            {
            case MFMailComposeResult.cancelled.rawValue:
                alert("Ooops", msg: "Mail canceled")
            case MFMailComposeResult.sent.rawValue:
                alert("Yes!", msg: "Mail sent")
            case MFMailComposeResult.saved.rawValue:
                alert("Yes!", msg: "Mail saved")
            case MFMailComposeResult.failed.rawValue:
                alert("Oooops!", msg: "Mail failed")
            default: break
            }
            
            dismiss(animated: true, completion: nil)
        }
        
        
        @IBAction func sendButtonClicked(_ sender: AnyObject)
        {
            let imageName = "image1.png"
            showMailComposerWith(imageName)
        }
        
        fileprivate func alert(_ title: String, msg: String)
        {
            let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }

        
        // gets called when the view is loaded
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            
        }
        
        
        
        
        
}




















