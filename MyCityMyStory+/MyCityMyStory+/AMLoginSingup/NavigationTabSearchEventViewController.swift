//
//  NavigationTabSearchEventViewController.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/5/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit



class NavigationTabSearchEventViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

extension NavigationTabSearchEventViewController: MIPivotRootPage {
    
    func imageForPivotPage() -> UIImage? {
        return UIImage(named: "iconSearch")
    }
    
}
