//
//  SendNotificationAllUsers.swift
//  MyCityMyStory+
//
//  Created by ESPRIT on 26/12/2016.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Alamofire
class SendNotificationAllUsers: NSObject {

    static let  IOS_NOTIFICATION_URL : String = "https://fcm.googleapis.com/fcm/send"
    static let IOS_NOTIFICATION_KEY : String = "key=AAAA4WPNRcA:APA91bHzDePWhxMXGpo-ltrxajgfiySBN3gYTisACxc_n-eZqFCyAtyrMAWhvPZdxyy6h_nB2lZV80Wda2IrkPjooI3-cWofbmGoYmIDtV_oWbn3FWcprMuSsT4SaCOLTgSWcgMZtnXmq0hZvpUuw7wjGorl7yqzQw"
    static let  CONTENT_TYPE : String = "application/json"
    static func sendNotif ( deviceToken : String ,message : String,title : String){
        let headers: [String:String] = [
            "Content-type": CONTENT_TYPE,
            "Authorization": IOS_NOTIFICATION_KEY
        ]
        let parameters : [String:Any]  = ["to":deviceToken,"notification":["title":title,"body":message]]
   
        Alamofire.request(IOS_NOTIFICATION_URL, method: .post , parameters : parameters, encoding: JSONEncoding.default,  headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                print("Notification Successful------ ",response)

            case .failure(let error):
                print("Notification ERROR------ ",error)
                //   print("In Function getAll Event "+String(self.tab.count))
                
            }
            }.resume()
 
        
        
    }
    
    
}
