//
//  NavigationController.swift
//  MIPivotPageController
//
//  Created by Mario on 17/09/16.
//  Copyright © 2016 Mario Iannotta. All rights reserved.
//

import UIKit

class NavigationControllerMap: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

extension NavigationControllerMap: MIPivotRootPage {
    
    func imageForPivotPage() -> UIImage? {
        return UIImage(named: "iconA")
    }
    
    
}
