//
//  GoogleMapViewController.swift
//  MyCityMyStory
//
//  Created by Chebbi on 11/5/16.
//  Copyright © 2016 Chebbi. All rights reserved.
//

import UIKit
import GoogleMaps


//AIzaSyAC6TFKkQiRLR9yWgRI-I_5tnicV5lnevA

class GoogleMapViewController: UIViewController , GMSMapViewDelegate {


        var  event : Event!
    
  
    @IBAction func DoneAction(_ sender: Any) {
       
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
  
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     GMSServices.provideAPIKey("AIzaSyAC6TFKkQiRLR9yWgRI-I_5tnicV5lnevA")
        
      
        
       let camera = GMSCameraPosition.camera(withLatitude: event.venue.latitude , longitude:event.venue.longitude , zoom: 20)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        
        // Creates a marker in the center of the map.
         let marker = GMSMarker()
         marker.position = CLLocationCoordinate2D(latitude: event.venue.latitude, longitude: event.venue.longitude)
         marker.title = event.venue.city
         marker.snippet = event.eventName
         marker.map = mapView
         view = mapView

 
        
     
    }
   
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
