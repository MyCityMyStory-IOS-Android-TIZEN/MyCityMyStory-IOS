//
//  DemoViewController.swift
//  TestCollectionView
//
//  Created by Alex K. on 12/05/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class DemoViewController: ExpandingViewController {
  
  typealias ItemInfo = (imageName: String, title: String)
  fileprivate var cellsIsOpen = [Bool]()
  fileprivate var items: [Event] = []
  
 
}

// MARK: life cicle

extension DemoViewController {
  
  override func viewDidLoad() {
    itemSize = CGSize(width: 256, height: 335)
    super.viewDidLoad()


    let rootRef = FIRDatabase.database().reference()
    var currentUserCountry : String = ""
    var currentUserCity : String = ""
     var stateString : String = ""
    

    rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!).observe(.value, with: { snapshot in
        currentUserCountry = (snapshot.childSnapshot(forPath: "country").value as? String)!
        var ss : String = ""
        
        currentUserCity = (snapshot.childSnapshot(forPath: "city").value as? String)!
        Me.start { (me) in
            print("1")
            print("currentUserCity ",currentUserCity.trimmingCharacters(in: CharacterSet.whitespaces))

                let url = NSURL(string: "https://elchebbi-ahmed.alwaysdata.net/mycitymystory/getStateOfCountry.php?city="+currentUserCity.replacingOccurrences(of: " ", with: "%20"))
            if url != nil {
                URLSession.shared.dataTask(with: url as! URL) { (data, response, error)-> Void in
                    if error != nil{
                        print(error ?? "")
                        return
                    }
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                        print(json)
                        
                        for dictionary in json as! [[String: AnyObject]]{
                            
                            ss =  dictionary["state"] as! String
                        }
                        print("sssssssss ",ss)
                        sharedVar.currentUserCity = currentUserCity
                        sharedVar.currentUserCountry = currentUserCountry
                        sharedVar.currentUserState = ss
                        do {
                            me.runNext()
                            
                        }
                    } catch let jsonError{
                        print(jsonError)
                    }
                    
                    }.resume()

            }else{
                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                    print("INDEX : \(index)")
                    
                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                   strMessage: "Check your connection",
                   btnArray: ["Yup I get It"], strTitle: "Network Error")
            }
            
            
            }.next { (caller, me) in
                  print("2 : ",currentUserCity,"/",ss,"/",currentUserCountry)
                self.items=[]
                let headers: HTTPHeaders = [
                    "Ocp-Apim-Subscription-Key": "72848bd8222a46e8a7de8626e7c3f9fd"
                ]
                
                Alamofire.request("https://api.allevents.in/events/list/?city="+currentUserCity.replacingOccurrences(of: " ", with: "%20")+"&state="+ss+"&country="+currentUserCountry.replacingOccurrences(of: " ", with: "%20"), method: .post ,headers: headers).responseJSON { response in
                    switch response.result {
                    case .success:
                        print("Validation Successful")
                        if let json = response.result.value{
                            print(json)
                            let jsonResult:Dictionary = json  as! [String: Any]
                            let data : NSArray = (jsonResult["data"] as! NSArray)
                            
                            for l in data  {
                                let ev  : Event = Event(dic: l as! [String:Any])
                                
                                
                                self.items.append(ev)
                            }
                            print("size items = ",self.items.count)
                            if self.items.count == 0 {
                                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                    print("INDEX : \(index)")
                                    
                                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                   strMessage: "We Are So Sorry your City or Country, have no event in our data base, your can change your city",
                                   btnArray: ["Yup I get It"], strTitle: "No Event")

                            }
                            
                            
                            
                            self.registerCell()
                            self.fillCellIsOpeenArry()
                            self.addGestureToView(self.collectionView!)
                            //  self.configureNavBar()
                            self.collectionView?.reloadData()
                             me.end()
                        }
                        
                    case .failure(let error):
                        print(error)
                        //   print("In Function getAll Event "+String(self.tab.count))
                        
                    }
                    }.resume()
                

              

                 //We are done here
               
            }.run()
        
     })
    
    
  

    
    
    
    
  }
    override func viewWillAppear(_ animated: Bool) {
        print("**********viewWillAppear")

    }
    /*********************/
    func getAllEvent(city : String,state : String,country : String)   {
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
}

// MARK: Helpers 

extension DemoViewController {
  
  fileprivate func registerCell() {
    
    let nib = UINib(nibName: String(describing: DemoCollectionViewCell.self), bundle: nil)
    collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: DemoCollectionViewCell.self))
  }
  
  fileprivate func fillCellIsOpeenArry() {
    for _ in items {
      cellsIsOpen.append(false)
    }
  }
  
    fileprivate func getViewController(index : IndexPath) -> ExpandingTableViewController {
    let storyboard = UIStoryboard(storyboard: .Main)
    let toViewController: DemoTableViewController = storyboard.instantiateViewController()
    toViewController.event = items[index.row]
     return toViewController
  }
  
  fileprivate func configureNavBar() {
    navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
  }
}

/// MARK: Gesture

extension DemoViewController {
  
  fileprivate func addGestureToView(_ toView: UIView) {
    let gesutereUp = Init(UISwipeGestureRecognizer(target: self, action: #selector(DemoViewController.swipeHandler(_:)))) {
      $0.direction = .up
    }
    
    let gesutereDown = Init(UISwipeGestureRecognizer(target: self, action: #selector(DemoViewController.swipeHandler(_:)))) {
      $0.direction = .down
    }
    toView.addGestureRecognizer(gesutereUp)
    toView.addGestureRecognizer(gesutereDown)
  }

  func swipeHandler(_ sender: UISwipeGestureRecognizer) {
    let indexPath = IndexPath(row: currentIndex, section: 0)
    guard let cell  = collectionView?.cellForItem(at: indexPath) as? DemoCollectionViewCell else { return }
    // double swipe Up transition
    if cell.isOpened == true && sender.direction == .up {
        pushToViewController(getViewController(index : indexPath))
      
      if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
        rightButton.animationSelected(true)
      }
    }
    
    let open = sender.direction == .up ? true : false
    cell.cellIsOpen(open)
    cellsIsOpen[(indexPath as NSIndexPath).row] = cell.isOpened
  }
}

// MARK: UIScrollViewDelegate 

extension DemoViewController {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    
    }
}

// MARK: UICollectionViewDataSource

extension DemoViewController {
  
  override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
    guard let cell = cell as? DemoCollectionViewCell else { return }

    let index = (indexPath as NSIndexPath).row % items.count
    let info = items[index]
    
    /***********/
    /***/
    let thumbnailURL = URL(string: items[indexPath.row].thumbUrlLarge)
    let networkService = NetworkService(url: thumbnailURL!)
    
    
    networkService.downloadImage { (imageData) in
        let image = UIImage(data: imageData as Data)
        DispatchQueue.main.async(execute: {
            cell.backgroundImageView?.image = image
        })
    }

    /***********/
    
     cell.customTitle.text = info.eventName
    cell.cellIsOpen(cellsIsOpen[index], animated: false)
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
    guard let cell = collectionView.cellForItem(at: indexPath) as? DemoCollectionViewCell
          , currentIndex == (indexPath as NSIndexPath).row else { return }
    

    if cell.isOpened == false {
      cell.cellIsOpen(true)
      
    } else {
        pushToViewController(getViewController(index : indexPath))
      
      if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
        rightButton.animationSelected(true)
      }
    }
  }
    
   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : DemoTableViewController = segue.destination as! DemoTableViewController
        svc.event = sender as! Event
    }*/

}

// MARK: UICollectionViewDataSource
extension DemoViewController {
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DemoCollectionViewCell.self), for: indexPath)
  }
    
}
