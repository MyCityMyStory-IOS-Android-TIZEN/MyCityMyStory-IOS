//
//  AddEventViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/10/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import  FirebaseDatabase
import MapKit


class AddEventViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , WWCalendarTimeSelectorProtocol,UIGestureRecognizerDelegate  {
    let rootRef = FIRDatabase.database().reference()
    let storage = FIRStorage.storage()
    var id :String!
    var event = Event()
    
    var textCategory : String = ""
    
    @IBOutlet var ImgEvent: UIImageView!
    
 
    
    @IBAction func AlertCategory(_ sender: Any) {
        
        
        MTPopUp(frame: self.view.frame).show(complete: { (index) in
            print("INDEX : \(index)")
            switch index {
            case 1:
                self.textCategory = "Business"
            case 2:
                self.textCategory = "Concerts"
            case 3:
                self.textCategory = "Exhibitions"
            case 4:
                self.textCategory = "Festivale"
            case 5:
                self.textCategory = "Meetups"
            case 6:
                self.textCategory = "Music"
            case 7:
                self.textCategory = "Parties"
            case 8:
                self.textCategory = ""
            default:
                 self.textCategory = ""
            }
        }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
           strMessage: "Choice the category of your event",
           btnArray: ["Business","Concerts","Exhibitions","Festivale","Meetups","Music","Parties","Nope"], strTitle: "Category")
        
        
    }
    
    
    @IBOutlet weak var tvNbPlace: AMInputView!

    var TvPlace : String = ""
    
    @IBOutlet weak var tvDesc: AMInputView!

    @IBOutlet weak var tvTitle: AMInputView!
    
    let ref: FIRDatabaseReference? = nil
    
    
    /***************/
    var testPhoto : Bool = false
    var testMap : Bool = false
    /****************/
    
    /**********/
    // Add Date Time Start & End
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    private var dateStartString : String = ""
    private var dateEndString : String = ""
    private var selectDateStart : Bool = false
    private var selectDateEnd : Bool = false
    private var isSelectedDateStart : Bool = false
    private var isSelectedDateEnd : Bool = false
    /**********/
    

    /**********/
    // map view

    @IBOutlet var mapView: MKMapView!
    var countryy : String = ""
    var cityy : String = ""
    var streett : String = ""
    var lon :  Double = 0.0
    var lat : Double = 0.0

    /**********/
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.isHidden = true
        let recognizer = UITapGestureRecognizer(target: self, action:Selector("Handle:"))
        recognizer.delegate = self
        mapView.addGestureRecognizer(recognizer)
        self.tabBarController?.tabBar.isHidden = true

        
    }
    @IBAction func SelectImageAction(_ sender: Any) {
        
        
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        
        self.show(myPickerController, sender: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        ImgEvent.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if ImgEvent.image != nil  {
            testPhoto = true
        }else {
            testPhoto = false
        }
       // picker.popViewController(animated: true)
       
        picker.dismiss(animated: true, completion: nil)
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SaveEventAction(_ sender: AnyObject) {
        
        
        AddEvent()
      

    }
    
    @IBAction func CancelEventAction(_ sender: AnyObject) {
        
        
        dismiss(animated: true, completion: nil)

        
    }
    
  
    /******************Start Verif Number of place is only number **********/
    private func isOnlyNumber(number : String) -> Bool{
         let badCharacters = NSCharacterSet.decimalDigits.inverted
        
        if number.rangeOfCharacter(from: badCharacters) == nil {
              return true
        } else {
            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                print("INDEX : \(index)")
                
            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
               strMessage: "Erreur Number of place should be a number",
               btnArray: ["Yup I get It"], strTitle: "Error")
            
              return false
        }
      
    }
    /******************End Verif Number of place is only number **********/
    /******************Start Verif The choice of Date Start & Date End **********/
    private func checkTwoDate(dateStart : String, dateEnd : String) -> Bool{
        //selectDateStart == true && selectDateEnd == true
        if isSelectedDateStart == false {
            
            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                print("INDEX : \(index)")
                
            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
               strMessage: "You should select your start date time of your event ",
               btnArray: ["Yup I get It"], strTitle: "Start Time")
            return false
            
        }else {
            if isSelectedDateEnd == false {
                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                    print("INDEX : \(index)")
                    
                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                   strMessage: "You should select your end date time of your event ",
                   btnArray: ["Yup I get It"], strTitle: "End Time")
                return false
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "d' 'MMMM' 'yyyy', 'h':'mma"
                let startTimeDate  = dateFormatter.date(from: dateStartString)!
                let  endTimeDate = dateFormatter.date(from: dateEndString)!
                
               /* if startTimeDate.compare(endTimeDate).  {
                    
                    return true
                }else{
                    MTPopUp(frame: self.view.frame).show(complete: { (index) in
                        print("INDEX : \(index)")
                        
                    }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                       strMessage: "You should select the correct start date time & end date time ",
                       btnArray: ["Yup I get It"], strTitle: "Opps")
                    return false
                }
                */
                let userCalendar = Calendar.current
                let userCalendarend = Calendar.current
                let hourStart = userCalendar.component(.hour, from: startTimeDate)
                let minutesStart = userCalendar.component(.minute, from: startTimeDate)
                let secondsStart = userCalendar.component(.second, from: startTimeDate)
                let dayStart = userCalendar.component(.day, from: startTimeDate)
                let monthStart = userCalendar.component(.month, from: startTimeDate)
                let yearStart = userCalendar.component(.year, from: startTimeDate)
                
                
                let hourEnd = userCalendarend.component(.hour, from: endTimeDate)
                let minutesEnd = userCalendarend.component(.minute, from: endTimeDate)
                let secondsEnd = userCalendarend.component(.second, from: endTimeDate)
                let dayEnd = userCalendarend.component(.day, from: endTimeDate)
                let monthEnd = userCalendarend.component(.month, from: endTimeDate)
                let yearEnd = userCalendarend.component(.year, from: endTimeDate)
           
                
                print(hourStart,hourStart,secondsStart,dayStart,monthStart,yearStart)
                print(hourEnd,hourEnd,secondsEnd,dayEnd,monthEnd,yearEnd)
                if yearStart == yearEnd && monthStart == monthEnd && dayStart == dayEnd
                    && hourStart == hourEnd && minutesStart == minutesEnd && secondsStart == secondsEnd {
                    MTPopUp(frame: self.view.frame).show(complete: { (index) in
                        print("INDEX : \(index)")
                        
                    }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                       strMessage: "Date  end  == Date Start",
                       btnArray: ["Yup I get It"], strTitle: "Check your Date")
                    return false
                }else{
                    if yearStart > yearEnd {
                        MTPopUp(frame: self.view.frame).show(complete: { (index) in
                            print("INDEX : \(index)")
                            
                        }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                           strMessage: "Year end  > Year Start",
                           btnArray: ["Yup I get It"], strTitle: "Check your Date")
                        return false
                    } else {
                        if monthStart > monthEnd {
                            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                print("INDEX : \(index)")
                                
                            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                               strMessage: "Month end  > Month Start",
                               btnArray: ["Yup I get It"], strTitle: "Check your Date")
                            return false
                        }else {
                            if dayStart > dayEnd {
                                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                    print("INDEX : \(index)")
                                    
                                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                   strMessage: "Day end  > Day Start",
                                   btnArray: ["Yup I get It"], strTitle: "Check your Date")
                                return false
                            }else {
                                if hourStart > hourEnd {
                                    
                                    MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                        print("INDEX : \(index)")
                                        
                                    }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                       strMessage: "hours end  > hours Start",
                                       btnArray: ["Yup I get It"], strTitle: "Check your Date")
                                    return false
                                    
                                }else{
                                    if minutesStart > minutesEnd {
                                        MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                            print("INDEX : \(index)")
                                            
                                        }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                           strMessage: "Minutes end  > Minutes Start",
                                           btnArray: ["Yup I get It"], strTitle: "Check your Date")
                                        return false
                                    }else {
                                        if secondsEnd > secondsStart {
                                            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                                print("INDEX : \(index)")
                                                
                                            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                               strMessage: "seconds end  > seconds Start",
                                               btnArray: ["Yup I get It"], strTitle: "Check your Date")
                                            return false
                                        }
                                        else{
                                            return true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                
                
                
            }
        }
    
        

    }
    /******************end Verif The choice of Date Start & Date End**********/

    func AddEvent (){
        if testPhoto == true {
            if tvTitle.textFieldView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
            
                if tvDesc.textFieldView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
                    
                    if  testMap == true {
                       
                        if textCategory != "" {
                            if tvNbPlace.textFieldView.text != ""{
                                if    isOnlyNumber(number: (tvNbPlace.textFieldView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!) == true
                                    
                                    &&  checkTwoDate(dateStart: dateStartString, dateEnd: dateEndString) == true   {
                                    
                                    let Tittle = tvTitle.textFieldView.text
                                    
                                    print("title"+Tittle!)
                                    let Category = textCategory
                                    print("Category"+Category)
                                    
                                    
                                    let NbPlace = Int(tvNbPlace.textFieldView.text!)
                                    // print("NbPlace"+NbPlace)
                                    let Desc = tvDesc.textFieldView.text
                                    let Place = TvPlace
                                    print("Place"+Place)
                                    let eventItem = Event (eventName: Tittle!, categories: Category, placee: Place, nbPlace: NbPlace! ,DescEvent :  Desc!,nbrPlaceDispo : NbPlace!)        // 3
                                    let eventItemRef = rootRef.child("events").childByAutoId()
                                    //eventItemRef.setValue(eventItem.toAnyObject())
                                    id = eventItemRef.key
                                    UploadImage()
                                    
                                    
                                    //self.dismiss(animated: true, completion: nil)
                                    
                                    
                                }

                            
                            }else {
                                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                    print("INDEX : \(index)")
                                    
                                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                                   strMessage: "Erreur you should number place ",
                                   btnArray: ["Yup I get It"], strTitle: "number place")
                                
                            }

                            
                        }else {
                            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                                print("INDEX : \(index)")
                                
                            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                               strMessage: "Erreur you should put your  Category",
                               btnArray: ["Yup I get It"], strTitle: "Category")
                            
                        }

                        
                    }else {
                        MTPopUp(frame: self.view.frame).show(complete: { (index) in
                            print("INDEX : \(index)")
                            
                        }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                           strMessage: "Erreur you should put your Location in map",
                           btnArray: ["Yup I get It"], strTitle: "Map")
                        
                    }

                    
                }else {
                    MTPopUp(frame: self.view.frame).show(complete: { (index) in
                        print("INDEX : \(index)")
                        
                    }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                       strMessage: "Erreur you should put your event description",
                       btnArray: ["Yup I get It"], strTitle: "descripition Event")
                    
                }
                
            }else {
                MTPopUp(frame: self.view.frame).show(complete: { (index) in
                    print("INDEX : \(index)")
                    
                }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
                   strMessage: "Erreur you should put your event name",
                   btnArray: ["Yup I get It"], strTitle: "Event Name")
                
            }
            
        }else {
            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                print("INDEX : \(index)")
                
            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
               strMessage: "Erreur Event pict",
               btnArray: ["Yup I get It"], strTitle: "Event Pict")
            

        }
        
        
        
    }
    
    func UploadImage(){
        
        // Data in memory
        var data = NSData()
        data = UIImageJPEGRepresentation(ImgEvent.image!, 0.8)! as NSData
        
        // Create a reference to the file you want to upload
        let imageRef = storage.reference().child("images_events").child(id)
        let metaData = FIRStorageMetadata()
        metaData.contentType = "image/jpg"
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = imageRef.put(data as Data, metadata: metaData){(metaData,error) in
            if let error = error {
                print(error.localizedDescription)
                
 
                // Uh-oh, an error occurred!
            } else {
                // Metadata contains file metadata such as size, content-type, and download URL.
                let downloadURL = metaData!.downloadURL()!.absoluteString
                
                
                
                let Tittle = self.tvTitle.textFieldView.text
                print("title"+Tittle!)
                let Category = self.textCategory
                print("Category"+Category)
                
                let NbPlace = Int(self.tvNbPlace.textFieldView.text!)
                // print("NbPlace"+NbPlace)
                let Desc = self.tvDesc.textFieldView.text

                let post : [String : Any]  = [
                    "id": self.id,
                    "title": Tittle!,
                    "urlImage": ""+downloadURL,
                    "category": Category,
                    "place" : self.countryy,
                    "description" :Desc!,
                    "latitudeEvent" : self.lat ,
                    "longitudeEvent" : self.lon ,
                    "nbPlaces": Int(NbPlace!),
                    "nbAvailablePlaces": 0,
                    "user_id" : FIRAuth.auth()?.currentUser?.uid,
                    "startDate" : self.dateStartString,
                    "endDate" : self.dateEndString,
                    
                    ]
                self.rootRef.child("events").child(self.id).setValue(post)
                
                /***********Send notification for All users have the same country*******************/
                
          
                
                
                    /******* For Each Token ******/
                
                            self.rootRef.child("Tokens").observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                        
                                                for snap in snapshots {
                                                    /*****For  User with TokenId ( User Id )***/
                                                    let tokenIdUser = snap.childSnapshot(forPath: "Token").value as! String
                                                    print(tokenIdUser)
                                                    self.rootRef.child("users").child(snap.key).observe(.value, with: { snapshot in
                                                        let user = EntityUser(snap: snapshot)
                                                        print(self.countryy)
                                                        if user.country == self.countryy {
                                                            
                                                            SendNotificationAllUsers.sendNotif(deviceToken: tokenIdUser, message: self.tvTitle.textFieldView.text!+" In "+self.countryy, title: "New Event : "+self.tvTitle.textFieldView.text!)
                                                            
                                                         }
                                                        
                                                        
                                                    })

                                                }
                                        }
                         }) { (error) in
                    print(error.localizedDescription)
                }
                

                    /****************************/
                
                /**********************************/
                self.navigationController?.popToRootViewController(animated: true)
                
            }
            
         }
 
    }
    
    
    /****************************/
    func selectDate() {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
      

        
        
        selector.optionShowTopPanel = true
        selector.optionShowTopContainer = true

        
        
        present(selector, animated: true, completion: nil)
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        if selectDateStart == true {
           dateStartString = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
            selectDateStart = false
        }
        if selectDateEnd == true {
            dateEndString = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
             selectDateEnd = false
        }
         // = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            //      dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //      dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    
    
    @IBAction func actionStartDate(_ sender: Any) {
        selectDateStart = true
        selectDate()
        isSelectedDateStart = true
    }
    
    @IBAction func actionEndDate(_ sender: Any) {
        selectDateEnd = true
        selectDate()
        isSelectedDateEnd = true
    }
    //******************select place **********//
    @IBAction func SelectPlaceAction(_ sender: Any) {
        mapView.isHidden = false

    }
  
    @IBAction func Handle(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        self.lat = coordinate.latitude
        self.lon = coordinate.longitude
        
        print("lo",location)
        print("co",coordinate)
        let loc = CLLocation(latitude: lat, longitude: lon)
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            // Address dictionary
            print(placeMark.addressDictionary)
            
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
                
            }
            
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
            }
            
            // City
            if let city = placeMark.addressDictionary!["City"] as?  NSString {
                self.cityy = city as String
                print("jjjj",city)
                print(self.cityy)
            }
            
            
            
            
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
                print("ziip",zip)
            }
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                 self.countryy = country as String
            }
            
            self.testMap = true
        })
        
        TvPlace = ("\(cityy) \(countryy) \(streett)")
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
    }
}






