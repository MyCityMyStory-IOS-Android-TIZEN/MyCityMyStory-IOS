//
//  ReaderCountry.swift
//  MyCityMyStory+
//
//  Created by ESPRIT on 25/12/2016.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import SwiftyJSON
class ReaderCountry: NSObject {

    static var  arrayCountys : [Country] =  [Country]()
    static func getCountrys()  {
        if let path = Bundle.main.path(forResource: "countriesToCities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = JSON(data: data)
                if jsonObj != JSON.null {
               
                    for j in jsonObj {
                        print("****Start*****")
                        var country : Country = Country()
                        country.name = j.0
                        var arraycountry : [String] = [String]()
                        for a in j.1 {
                             arraycountry.append("\(a.1)")
                        }
                        country.cities = arraycountry
                        arrayCountys.append(country)
                    }
                    
                    
                   
                } else {
                    print("Could not get json from file, make sure that file contains valid json.")
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
}
