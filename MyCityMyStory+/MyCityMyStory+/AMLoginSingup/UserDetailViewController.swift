//
//  UserDetailViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/17/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import FirebaseAuth
import  FirebaseDatabase

class UserDetailViewController: MIPivotPage ,SCPopDatePickerDelegate , UITextFieldDelegate{
    let datePicker = SCPopDatePicker()
    let date = Date()
    
    let rootRef = FIRDatabase.database().reference()
  
    static var isSelectedCountry = false
 
    @IBOutlet var tvFirstname: UITextField!
    
    @IBOutlet var tvSex: UISegmentedControl!
    
    @IBOutlet var tvBirthday: UILabel!

    @IBOutlet var tvCity: UILabel!
    
    @IBOutlet var tvCountry: UILabel!

    
    @IBOutlet var tvPhone: UITextField!
     
    @IBOutlet var tvDate: UILabel!

    let ref: FIRDatabaseReference? = nil

    @IBOutlet var tvLastName: UITextField!
     static var countrySelected = ""
    static var citySelected = ""
    static var countrySelectedIndex = 0
    static var citySelectedIndex = 0

    static var selectedFromUserDetail = false
    var urlImageUser : String = ""
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
       // for hitting return
      tvPhone.delegate = self
      tvFirstname.delegate = self
        tvLastName.delegate = self
        //****
        // for tapping
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard")))
      
        
        
        
 
        print("pass Detail view User")
       
            if ( rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!) != nil ){
                
                
                rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { snapshot in
                    self.tvCity.text = snapshot.childSnapshot(forPath: "city").value as! String
                    
                    self.tvFirstname.text = snapshot.childSnapshot(forPath: "firstname").value as! String
                    self.tvLastName.text = snapshot.childSnapshot(forPath: "lastname").value as! String
                    self.tvCountry.text = snapshot.childSnapshot(forPath: "country").value as! String
                    self.tvPhone.text = snapshot.childSnapshot(forPath: "phone").value as! String
                    let s =  snapshot.childSnapshot(forPath: "sexe").value as! String
                    
                    if s == "Woman" {
                         self.tvSex.selectedSegmentIndex = 1
                    }else{
                        self.tvSex.selectedSegmentIndex = 0
                    }
                    self.tvBirthday.text = snapshot.childSnapshot(forPath: "birthday").value as! String
                    // self.setNavigationBarItem()
                    self.urlImageUser =  snapshot.childSnapshot(forPath: "urlImageUser").value as! String
                    
                })
                
                
                
                
                
                
                
                
                
            }

         
        
       
        // Do any additional setup after loading the view.
    }
   
    // for tapping
    func dismissKeyboard() {
        tvPhone.resignFirstResponder()
        tvFirstname.resignFirstResponder()
        tvLastName.resignFirstResponder()
    }
    
    // for hitting return
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        tvPhone.resignFirstResponder()
        tvFirstname.resignFirstResponder()
        tvLastName.resignFirstResponder()

        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tvCountry.text = UserDetailViewController.countrySelected
        tvCity.text = UserDetailViewController.citySelected
        
        
    }

    @IBAction func changeDateBirthday(_ sender: Any) {
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.date
        self.datePicker.showBlur = true
        self.datePicker.datePickerStartDate = self.date
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = UIColor.darkGray
        self.datePicker.showCornerRadius = false
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)

        
    }
    func scPopDatePickerDidSelectDate(_ date: Date) {
         tvBirthday.text = date.stringFromFormat("MM'/'dd'/'yyyy'")
    }

    @IBAction func btnAdd(_ sender: AnyObject) {
        
        if tvFirstname.text != "" && tvLastName.text != "" && tvPhone.text != ""
            && tvCity.text != "" && tvCountry.text != "" && tvBirthday.text != "" {
            let firstname = tvFirstname.text
            let lastName = tvLastName.text
            var sex = "Man"
            if (tvSex.selectedSegmentIndex == 1){
                sex = "Woman"
            }
            let phone = tvPhone.text
            let city = tvCity.text
            let country = tvCountry.text
            
            
            let dateString = tvBirthday.text
            
            // 2
            let userItem = EntityUser(Firstname: firstname!, Lastname: lastName!, city: city!
                ,country :country!,sexe :sex,phone : phone!,birthday : dateString!, urlImage : urlImageUser)       // 3
             let eventItemRef = rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!)
            
            // 4
            eventItemRef.setValue(userItem.toAnyObject())
            
            
            
            self.navigationController?.popToRootViewController(animated: true)
        }else  {
            MTPopUp(frame: self.view.frame).show(complete: { (index) in
                print("INDEX : \(index)")
                
            }, view: self.view, animationType: MTAnimation.ZoomIn_ZoomOut,
               strMessage: "Erreur you should check all field is not empty",
               btnArray: ["Yup I get It"], strTitle: "Erreur")
        }
        
        
       
       
       
        
     }
    
  
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func selectCountry(_ sender: Any) {
        UserDetailViewController.isSelectedCountry = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController!
        UserDetailViewController.citySelected = ""
    

        
        pivotPageController.present(vc!, animated: true, completion: nil)
  
    
    }
    
    
    
    @IBAction func selectCity(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CityViewController") as! CityViewController!
        
        pivotPageController.present(vc!, animated: true, completion: nil)
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}
 
