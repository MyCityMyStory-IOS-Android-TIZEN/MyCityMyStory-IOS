//
//  MapEventViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/17/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import MapKit

class MapEventViewController: UIViewController ,MKMapViewDelegate{

    @IBOutlet weak var mapEvent: MKMapView!
  
    
    
    var event : Event!

    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        
        mapEvent.delegate = self
        let pin = MKPointAnnotation()
        pin.coordinate.latitude = event.venue.latitude
        pin.coordinate.longitude = event.venue.longitude
        pin.title = event.place
        
        mapEvent.addAnnotation(pin)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  self.setNavigationBarItem()
        
    }

    
}

