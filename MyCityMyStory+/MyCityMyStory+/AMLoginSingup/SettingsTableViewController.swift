//
//  SettingsTableViewController.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/6/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Firebase
import MessageUI
class SettingsTableViewController: MIPivotPage  , UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
//
    
    
    
    
    
    
    
    let SettingsArray : [String] = ["Your Interrest","Send FeedBack","Edit your Profile"]
    let SettingsArraySectoion2 : [String] = ["LogOut"]
    let SettingsArraySectoion3 : [String] = ["About"]
   
    
    @IBOutlet weak var tableView: UITableView!
     override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
       
      
 

    }
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
        return SettingsArray.count
        }
        
        else if section == 1 {
            return SettingsArraySectoion2.count
        }else if section == 2 {
            return SettingsArraySectoion3.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCellSettings")!
        if ( indexPath.section == 0){
            let lblUserName:UILabel = cell.viewWithTag(102) as! UILabel
            lblUserName.text = SettingsArray[indexPath.row]
            
            let imageIcon:UIImageView = cell.viewWithTag(101) as! UIImageView
            imageIcon.image = UIImage(named: "Settings\(indexPath.row).png")

        }
        else if indexPath.section == 1 {
            let lblUserName:UILabel = cell.viewWithTag(102) as! UILabel
            lblUserName.text = SettingsArraySectoion2[indexPath.row]
            
            let imageIcon:UIImageView = cell.viewWithTag(101) as! UIImageView
            imageIcon.image = UIImage(named: "Settings\(indexPath.section)\(indexPath.row).png")

        }
        else if indexPath.section == 2 {
            let lblUserName:UILabel = cell.viewWithTag(102) as! UILabel
            lblUserName.text = SettingsArraySectoion3[indexPath.row]
            
            let imageIcon:UIImageView = cell.viewWithTag(101) as! UIImageView
            imageIcon.image = UIImage(named: "Settings\(indexPath.section)\(indexPath.row).png")
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListCatViewController") as! ListCatViewController!
                pivotPageController.present(vc!, animated:true, completion:nil)
            
            }
            
            if indexPath.row == 1 {
                
                let imageName = "image1.png"
                showMailComposerWith(imageName)
                
                //   performSegue(withIdentifier: "SegueDetailSettingsSendFeedBack", sender: nil)
            }
            if indexPath.row == 2 {
                
                 let vc = self.storyboard?.instantiateViewController(withIdentifier: "userDetailProfile") as! UserDetailViewController!
                self.show(vc!, sender: nil)
            }

        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                if FIRAuth.auth()?.currentUser != nil {
                    
                    
                    do{
                        
                        try  FIRAuth.auth()?.signOut()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController!
                        self.present(vc!, animated: false, completion: nil )
                        
                    }catch let error as NSError{
                        print(error.localizedDescription)
                        
                    }
                    
                    
                    
                }
                
                
                
            }

        }
        
        if indexPath.section == 2 {
            
             if indexPath.row == 0  {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSViewController") as! AboutUSViewController!
                pivotPageController.present(vc!, animated : true ,completion:  nil)
             }

        }
     }
    
    
    /*
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let svc : DetailViewController = segue.destination as! DetailViewController
     svc.event = sender as! Event
     }*/
    
    
    // declare MIME types (Multipurpose Internet Mail Extension)
    // it defines which kind of information to send via email
    
    fileprivate enum MIMEType: String {
        case jpg = "image/jpeg"
        case png = "image/png"
        case doc = "application/msword"
        case ppt = "application/vnd.ms-powerpoint"
        case html = "text/html"
        case pdf = "application/pdf"
        
        init?(type: String) {
            switch type.lowercased() {
            case "jpg": self = .jpg
            case "png": self = .png
            case "doc": self = .doc
            case "ppt": self = .ppt
            case "html": self = .html
            case "pdf": self = .pdf
            default: return nil
            }
        }
    }
    
    // MARK: - Share Quotes via Email
    
    func showMailComposerWith(_ attachmentName: String)
    {
        if MFMailComposeViewController.canSendMail() {
            let subject = "Great quote from Duc Tran at Developers Academy"
            let messageBody = "Hey , Duc at DA here. This is very motivational!"
            let toRecipients = ["Ahmed.chebbi@esprit.tn"]
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            mailComposer.setSubject(subject)
            mailComposer.setMessageBody(messageBody, isHTML: false)
            mailComposer.setToRecipients(toRecipients)
            
            let fileParts = attachmentName.components(separatedBy: ".")
            let fileName = fileParts[0]
            let fileExtension = fileParts[1]
            
            if let filePath = Bundle.main.path(forResource: fileName, ofType: fileExtension) {
                if let fileData = try? Data(contentsOf: URL(fileURLWithPath: filePath)), let mimeType = MIMEType(type: fileExtension) {
                    mailComposer.addAttachmentData(fileData, mimeType: mimeType.rawValue, fileName: fileName)
                    
                    pivotPageController.present(mailComposer, animated : true ,completion:  nil)
                }
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        switch result.rawValue
        {
        case MFMailComposeResult.cancelled.rawValue:
            alert("Ooops", msg: "Mail canceled")
        case MFMailComposeResult.sent.rawValue:
            alert("Yes!", msg: "Mail sent")
        case MFMailComposeResult.saved.rawValue:
            alert("Yes!", msg: "Mail saved")
        case MFMailComposeResult.failed.rawValue:
            alert("Oooops!", msg: "Mail failed")
        default: break
        }
        
        
        pivotPageController.dismiss(animated: true, completion: nil)
    }
    
       
    fileprivate func alert(_ title: String, msg: String)
    {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        pivotPageController.present(alertController, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
   
    

}
