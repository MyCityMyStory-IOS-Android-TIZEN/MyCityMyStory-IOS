//
//  DemoViewController.swift
//  TestCollectionView
//
//  Created by Alex K. on 12/05/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import MIFab

class ALLEventVController: ExpandingViewController {
    fileprivate var fab: MIFab!

    let rootref = FIRDatabase.database().reference()

    
    typealias ItemInfo = (imageName: String, title: String)
    fileprivate var cellsIsOpen = [Bool]()
    fileprivate var items: [Event] = []
    
    @IBAction func showEvent(_ sender: Any) {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEventViewController") as! AddEventViewController!
        self.show(vc!, sender: nil)
    }

}

// MARK: life cicle

extension ALLEventVController {
    
    override func viewDidLoad() {
        itemSize = CGSize(width: 256, height: 290)
        super.viewDidLoad()
        //  setupFab()
        
        //getAllEvents()
        
        
    }
  /*  fileprivate func setupFab() {
        
        var fabConfig = MIFab.Config()
        
        fabConfig.buttonImage = UIImage(named: "iconA")
        fabConfig.buttonBackgroundColor = UIColor.orange
        
        fab = MIFab(
            parentVC: self,
            config: fabConfig,
            options: [
                MIFabOption(
                    title: "Item two",
                    image: UIImage(named: "iconB"),
                    backgroundColor: UIColor.orange,
                    tintColor: UIColor.white,
                    actionClosure: {
                        
                        let alertController = UIAlertController(title: "Demo", message: "First fab button tapped", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                }
                ),
                MIFabOption(
                    title: "Item 3",
                    image: UIImage(named: "iconc"),
                    backgroundColor: UIColor.orange,
                    tintColor: UIColor.white,
                    actionClosure: {
                        
                        let alertController = UIAlertController(title: "Demo", message: "First fab button tapped", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                }
                )
            ]
        )
        
        fab.showButton(animated: true)
        
    }*/

    override func viewWillAppear(_ animated: Bool) {
         items   = [Event]()
         getAllEvents()
    }
    func getAllEvents()   {
        
        rootref.child("events").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    self.items.append(Event(snap: snap))
                }
                print("size items = ",self.items.count)
                
                self.registerCell()
                self.fillCellIsOpeenArry()
                self.addGestureToView(self.collectionView!)
                //  self.configureNavBar()
                self.collectionView?.reloadData()
                
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        
    }
    

    
}

// MARK: Helpers

extension ALLEventVController {
    
    fileprivate func registerCell() {
        
        let nib = UINib(nibName: String(describing: DemoCollectionViewCell.self), bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: DemoCollectionViewCell.self))
    }
    
    fileprivate func fillCellIsOpeenArry() {
        for _ in items {
            cellsIsOpen.append(false)
        }
    }
    
    fileprivate func getViewController(index : IndexPath) -> ExpandingTableViewController {
        let storyboard = UIStoryboard(storyboard: .Main)
        let toViewController: ALLEventTableVController = storyboard.instantiateViewController()
        toViewController.event = items[index.row]
        return toViewController
    }
    
    fileprivate func configureNavBar() {
        navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
}

/// MARK: Gesture

extension ALLEventVController {
    
    fileprivate func addGestureToView(_ toView: UIView) {
        let gesutereUp = Init(UISwipeGestureRecognizer(target: self, action: #selector(ALLEventVController.swipeHandler(_:)))) {
            $0.direction = .up
        }
        
        let gesutereDown = Init(UISwipeGestureRecognizer(target: self, action: #selector(ALLEventVController.swipeHandler(_:)))) {
            $0.direction = .down
        }
        toView.addGestureRecognizer(gesutereUp)
        toView.addGestureRecognizer(gesutereDown)
    }
    
    func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        let indexPath = IndexPath(row: currentIndex, section: 0)
        guard let cell  = collectionView?.cellForItem(at: indexPath) as? DemoCollectionViewCell else { return }
        // double swipe Up transition
        if cell.isOpened == true && sender.direction == .up {
            pushToViewController(getViewController(index : indexPath))
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(true)
            }
        }
        
        let open = sender.direction == .up ? true : false
        cell.cellIsOpen(open)
        cellsIsOpen[(indexPath as NSIndexPath).row] = cell.isOpened
    }
}

// MARK: UIScrollViewDelegate

extension ALLEventVController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     //   pageLabel.text = "\(currentIndex+1)/\(items.count)"
    }
}

// MARK: UICollectionViewDataSource

extension ALLEventVController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        guard let cell = cell as? DemoCollectionViewCell else { return }
        
        let index = (indexPath as NSIndexPath).row % items.count
        let info = items[index]
        
        /***********/
        /***/
        let thumbnailURL = URL(string: items[indexPath.row].urlImage)
        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
                cell.backgroundImageView?.image = image
            })
        }
        
        /***********/
        
        cell.customTitle.text = info.eventName
        cell.cellIsOpen(cellsIsOpen[index], animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DemoCollectionViewCell
            , currentIndex == (indexPath as NSIndexPath).row else { return }
        
        
        if cell.isOpened == false {
            cell.cellIsOpen(true)
            
        } else {
            pushToViewController(getViewController(index : indexPath))
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(true)
            }
        }
    }
    
    /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let svc : DemoTableViewController = segue.destination as! DemoTableViewController
     svc.event = sender as! Event
     }*/
    
}

// MARK: UICollectionViewDataSource
extension ALLEventVController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DemoCollectionViewCell.self), for: indexPath)
    }
    
}
