//
//  Comments.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Chebbi on 11/9/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import Firebase

class Comments: NSObject {
    
    var id : String!
    var corp : String!
     var dateTimeComment: String!
    var event: String!
    var eventIdAllEvent: String!
    var eventIdFb: String!
    var user: String!
    
   init( snap :FIRDataSnapshot) {
               id = "\(snap.key)"
                corp="\(snap.childSnapshot(forPath: "corp").value as! String ) "
               dateTimeComment="\(snap.childSnapshot(forPath: "dateTimeComment") ) "
                event="\(snap.childSnapshot(forPath: "event") ) "
                eventIdAllEvent="\(snap.childSnapshot(forPath: "eventIdAllEvent") ) "
                eventIdFb="\(snap.childSnapshot(forPath: "eventIdFb") ) "
                user="\(snap.childSnapshot(forPath: "user") ) "
               
    }
 
}
