//
//  ViewController.swift
//  Maps Tutorial
//
//  Created by programming-xcode on 10/14/16.
//  Copyright © 2016 programming-xcode. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import RNNotificationView


class MapKitViewController: MIPivotPage, UISearchBarDelegate ,MKMapViewDelegate, CLLocationManagerDelegate  , UITableViewDelegate,UITableViewDataSource  {
    
    let searchController = UISearchController(searchResultsController: nil)

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var map: MKMapView!
    @IBOutlet var segmentedcontrol: UISegmentedControl!
    var manager = CLLocationManager()
    

       public  var tabEventNearMeSearch : [Event] = [Event] ()
    public  var tabEventNearMe : [Event] = [Event] ()
    
    /*********************/
    public  func getEventNearMe(Lat : Double,Long : Double)   {
        // Do any additional setup after loading the view, typically from a nib.
        
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": "72848bd8222a46e8a7de8626e7c3f9fd"
        ]
        
        Alamofire.request("https://api.allevents.in/events/geo/?latitude=\(Lat)&longitude=\(Long)&radius=5", method: .post ,headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value{
                    let jsonResult:Dictionary = json  as! [String: Any]
                    let data : NSArray = (jsonResult["data"] as! NSArray)
                    if (self.tabEventNearMe.count < 30 ){
                        for l in data  {
                            let ev  : Event = Event(dic: l as! [String:Any])
                            
                            let pin = MKPointAnnotation()
                            pin.coordinate.latitude = ev.venue.latitude
                            pin.coordinate.longitude =  ev.venue.longitude
                            pin.title = ev.eventName
                            
                            self.map.addAnnotation(pin)
                            
                            self.tabEventNearMe.append(ev)
                        }
                        
                        print(self.tabEventNearMe.count)

                    }
                    
                    
                }
                
            case .failure(let error):
                print(error)
                print("In Function getAll Event "+String(self.tabEventNearMe.count))
                
            }
            }.resume()
        
        
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        //Setup search bar
         searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        //Set delegate
        searchController.searchResultsUpdater = self
        //Add to top of table view
        tableView.tableHeaderView = searchController.searchBar
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
  
    
    /*******************/
    /*
        // Create Instance
        let notification = RNNotificationView()
        notification.titleFont = UIFont(name: "AvenirNext-Bold", size: 10)!
        notification.titleTextColor = UIColor.blue
        notification.show(withImage: nil,
                          title: "Title",
                          message: "Message",
                          onTap: {
                            print("Did tap notification")
        })
      */
        
        /**************/
    }
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        tabEventNearMeSearch = tabEventNearMe.filter { candy in
            return candy.eventName.lowercased().contains(searchText.lowercased())
        }
        
        self.tableView.reloadData()
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return tabEventNearMeSearch.count
        }
        return tabEventNearMe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell")!
        
        let event: Event
        if searchController.isActive && searchController.searchBar.text != "" {
            event = tabEventNearMeSearch[indexPath.row]
        } else {
            event = tabEventNearMe[indexPath.row]
        }
        
        cell.textLabel?.text = event.eventName + " City : " + event.location
        
       
        
        return cell
    }


    @IBAction func segmentchanged(_ sender: AnyObject) {
        switch segmentedcontrol.selectedSegmentIndex {
        case 0:
            map.mapType = MKMapType.standard
            break
        case 1:
            map.mapType = MKMapType.satellite
            break
        case 2:
            map.mapType = MKMapType.hybrid
            break
        case 4:
            map.mapType = MKMapType.hybridFlyover
            break
        case 3:
            map.mapType = MKMapType.satelliteFlyover
            break
        default:
            break
        }
    }
    
    
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        let userLocation:CLLocation = locations[0]
        
        let latitude:CLLocationDegrees = userLocation.coordinate.latitude
        
        let longitude:CLLocationDegrees = userLocation.coordinate.longitude
        
        let latDelta:CLLocationDegrees = 0.1
        
        let lonDelta:CLLocationDegrees = 0.1
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
     
        getEventNearMe(Lat: latitude, Long: longitude)
            self.tableView.reloadData()
        map.setRegion(region, animated: true)

       
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        pivotPageController.menuView.clipsToBounds = false
    
        
    }

    
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchController.isActive && searchController.searchBar.text != "" {
            performSegue(withIdentifier: "showDetailSearchEventNearMe", sender: tabEventNearMeSearch[indexPath.row])
        }else{
            performSegue(withIdentifier: "showDetailSearchEventNearMe", sender: tabEventNearMe[indexPath.row])
            
        }
     
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : DetailEventSearchNearMeViewController = segue.destination as! DetailEventSearchNearMeViewController
        svc.event = sender as! Event
    }
    
    
   /* // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailSearchEventNearMe" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let event: Event
                if searchController.isActive && searchController.searchBar.text != "" {
                    event = tabEventNearMeSearch[indexPath.row]
                } else {
                    event = tabEventNearMe[indexPath.row]
                }
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailEventSearchNearMeViewController
                controller.event = event
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }*/
    
    
    
}

extension MapKitViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)

    }
}

