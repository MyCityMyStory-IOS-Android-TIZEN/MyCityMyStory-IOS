//
//  ProfileViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/18/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth


class ProfileeViewController: UIViewController {
    
    let rootRef = FIRDatabase.database().reference()
    var user : EntityUser!
  
    
    var City : String = ""
    var Firstname : String = ""
    var Lastname : String = ""
    var Country : String = ""
    var Phone : String = ""
    var Sex : String = ""
    
    
    @IBOutlet weak var tvPhone: UILabel!

    @IBOutlet weak var tvSex: UILabel!
    @IBOutlet weak var tvCountry: UILabel!
    
    @IBOutlet weak var txxCity: UILabel!
    @IBOutlet weak var tvUsername: UILabel!
    @IBOutlet weak var tvLastname: UILabel!
    @IBOutlet weak var tvEmail: UILabel!
    //@IBOutlet weak var tvCity: UITextField!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { snapshot in
            self.City = snapshot.childSnapshot(forPath: "city").value as! String
            
            print("cityyyy"+self.City )
            self.Firstname = "ok"
            self.Lastname = "iii"
            //  self.Country = "kkk"
            self.Phone = "test"
            self.Sex = "test"
       
      //  self.setNavigationBarItem()
     
      
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  var userdata = [UserData]
  
    if ( rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!) != nil )
    {  rootRef.child("users").child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { snapshot in
            self.City = snapshot.childSnapshot(forPath: "city").value as! String
            
            print("cityyyy"+self.City )
            self.Firstname = snapshot.childSnapshot(forPath: "firstname").value as! String
            self.Lastname = snapshot.childSnapshot(forPath: "lastname").value as! String
            self.Country = snapshot.childSnapshot(forPath: "country").value as! String
            self.Phone = snapshot.childSnapshot(forPath: "phone").value as! String
            self.Sex = snapshot.childSnapshot(forPath: "sexe").value as! String
           
            
        //    self.setNavigationBarItem()
            
            
            self.txxCity.text = self.City
            self.tvEmail.text = FIRAuth.auth()?.currentUser?.email!
            self.tvSex.text = self.Sex
            self.tvPhone.text = self.Phone
            self.tvUsername.text = self.Firstname
            self.tvLastname.text = self.Lastname
            self.tvCountry.text = self.Country
            
        })
        }

        
        
        
    }
    



}
extension ProfileeViewController: MIPivotRootPage {
    
    func imageForPivotPage() -> UIImage? {
        return UIImage(named: "Profile")
    }
    
}
