//
//  MainPageMyCityMyStory.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 11/21/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

class MainPageMyCityMyStory: UIViewController {
    
    var window: UIWindow?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /**************Design Menu Tab***************/
      //  let myBlueColor = UIColor(red: 34/255.0, green: 167/255.0, blue: 240/255.0, alpha: 1)
       // let myBlueColor = ColorGold(hexString: "#ffe700ff")
        let myBlueColor = UIColor.darkGray
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let NavigationControllerMap = mainStoryboard.instantiateViewController(withIdentifier: "NavigationControllerMap") as! NavigationControllerMap
        /****/
        let ProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileUserViewController") as! ProfileUserViewController
        

        let settingsTabController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsNavigationController") as! SettingsNavigationController
        
        let tabBarControllerEventAcceuil = mainStoryboard.instantiateViewController(withIdentifier: "tabBarControllerEventAcceuil") as! NavigationTabControllerEvent
        
        let tabBarControllerEventSearchAPI = mainStoryboard.instantiateViewController(withIdentifier: "NavigationTabSearchEventViewController") as! NavigationTabSearchEventViewController
        
        
        NavigationControllerMap.navigationBar.isTranslucent = false
        NavigationControllerMap.navigationBar.barTintColor = myBlueColor
        NavigationControllerMap.navigationBar.tintColor = UIColor.white
        
      
        
            /****/
        
        
         let pivotPageController = MIPivotPageController.get(rootPages: [tabBarControllerEventAcceuil,ProfileViewController, NavigationControllerMap,tabBarControllerEventSearchAPI ,settingsTabController]) {
            
            $0.menuView.backgroundColor = myBlueColor
            $0.menuView.layer.shadowColor = UIColor.black.cgColor
            $0.menuView.layer.shadowOpacity = 0.3
            $0.menuView.layer.shadowOffset = CGSize(width: 0, height: 2)
            
            $0.setMenuHeight(60)
             $0.setLightStatusBar(true)
            
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.rootViewController = pivotPageController
        window?.makeKeyAndVisible()
        /*******************************************/
    }

}
