//
//  CountryViewController.swift
//  MyCityMyStory+
//
//  Created by ESPRIT on 25/12/2016.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

class CountryViewController: UIViewController,UISearchBarDelegate ,UITableViewDelegate,UITableViewDataSource {
    let searchController = UISearchController(searchResultsController: nil)

     var countryArray : [Country]  = [Country]()
    var tabEventNearMeSearch : [Country] = [Country] ()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        countryArray = ReaderCountry.arrayCountys
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        //Setup search bar
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        //Set delegate
        searchController.searchResultsUpdater = self
        tableView.tableHeaderView = searchController.searchBar
        
      

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        tabEventNearMeSearch = countryArray.filter { candy in
            return (candy.name?.lowercased().contains(searchText.lowercased()))!
        }
        
        self.tableView.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return tabEventNearMeSearch.count
        }
        
        return countryArray.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellCountry")!
        if searchController.isActive && searchController.searchBar.text != "" {
             cell.textLabel?.text = tabEventNearMeSearch[indexPath.row].name
        } else {
            cell.textLabel?.text = countryArray[indexPath.row].name
        }
        
       
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchController.isActive && searchController.searchBar.text != "" {
            var indexSelectedSeart : Int = 0
            for i in 0...(countryArray.count - 1) {
                if  countryArray[i].name ==  tabEventNearMeSearch[indexPath.row].name! {
                    indexSelectedSeart = i
                    break
                }
            }
            UserDetailViewController.countrySelected = countryArray[indexSelectedSeart].name!
            UserDetailViewController.countrySelectedIndex = indexSelectedSeart
            
            RegisterDetailUserController.countrySelected = countryArray[indexSelectedSeart].name!
            RegisterDetailUserController.countrySelectedIndex = indexSelectedSeart
            searchController.isActive = false
               self.dismiss(animated: true, completion : nil)
            
        }else{
            UserDetailViewController.countrySelected = countryArray[indexPath.row].name!
            UserDetailViewController.countrySelectedIndex = indexPath.row
            
            RegisterDetailUserController.countrySelected = countryArray[indexPath.row].name!
            RegisterDetailUserController.countrySelectedIndex = indexPath.row
             self.dismiss(animated: true, completion : nil)
            
        }

        

        
       
        
    
    }
    


}
extension CountryViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
    
}
