 
 
 import JTAppleCalendar

 import Alamofire
 
 class CalenderEventViewController: MIPivotPage  , UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    //@IBOutlet weak var monthLabel: UILabel!
    
 
    @IBOutlet weak var tableView: UITableView!
    
    var tabEventCalendar = [Event]()
    
    var numberOfRows = 6
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    let firstDayOfWeek: DaysOfWeek = .sunday
    let disabledColor = UIColor.clear
    let enabledColor = UIColor.blue
    let dateCellSize: CGFloat? = nil

    
    /*@IBAction func changeDirection(_ sender: UIButton) {
        for aButton in directions {
            aButton.tintColor = disabledColor
        }
        sender.tintColor = enabledColor
        
        if sender.title(for: .normal)! == "HorizontalCalendar" {
            calendarView.direction = .horizontal
        } else {
            calendarView.direction = .vertical
        }
        calendarView.reloadData()
    }*/
    
    
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        tabBarController?.tabBar.isHidden = false
        
        pivotPageController.menuView.clipsToBounds = false
     
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let myBlueColor = UIColor.darkGray
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = myBlueColor
        navigationController?.navigationBar.tintColor = UIColor.white
        
        
        
        formatter.dateFormat = "yyyy MM dd"
        
        // Setting up your dataSource and delegate is manditory
        // ___________________________________________________________________
        calendarView.delegate = self
        calendarView.dataSource = self
        
        // ___________________________________________________________________
        // Registering your cells is manditory
        // ___________________________________________________________________
        calendarView.registerCellViewXib(file: "CellView")
        
        // ___________________________________________________________________
        // Registering your cells is optional
        
        // ___________________________________________________________________
        calendarView.registerHeaderView(xibFileNames: ["PinkSectionHeaderView", "WhiteSectionHeaderView"])
        
        
        calendarView.cellInset = CGPoint(x: 0, y: 0)
        //        calendarView.allowsMultipleSelection = true
        
        calendarView.visibleDates { (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        
        tableView.delegate = self
        tableView.dataSource = self

        
        
    }
    
    func getEventByDate(city : String,state : String,country : String,DateStart : Int )   {
        // Do any additional setup after loading the view, typically from a nib.
        
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": "72848bd8222a46e8a7de8626e7c3f9fd"
        ]
        
        print("Date start in getEventByDate \(DateStart)")
        
        Alamofire.request("https://api.allevents.in/events/list/?city="+city+"&state="+state+"&country="+country+"&sdate=\(DateStart)", method: .post ,headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value{
                    let jsonResult:Dictionary = json  as! [String: Any]
                    let data : NSArray = (jsonResult["data"] as! NSArray)
                    
                    let dateSearch  = Date(timeIntervalSince1970: Double(DateStart))
                    let dayEventSearch = Calendar.current.component(.day, from: dateSearch)
                    let monthEventSearch =  Calendar.current.component(.month, from: dateSearch)
                    let yearEventSearch =  Calendar.current.component(.year, from:  dateSearch)

                    
                    
                    for l in data  {
                        let ev  : Event = Event(dic: l as! [String:Any])
                        
                        
                        let dayEvent = Calendar.current.component(.day, from: ev.startTime)
                        let monthEvent =  Calendar.current.component(.month, from: ev.startTime)
                        let yearEvent =  Calendar.current.component(.year, from: ev.startTime)
                        
                        if  dayEvent == dayEventSearch && monthEvent == monthEventSearch && yearEvent == yearEventSearch {
                             self.tabEventCalendar.append(ev)
                        }
                    
                    }
                //    print("In Function getAll Event "+String(self.tab.count))
                  //  self.tableView.reloadData()
                      self.tableView.reloadData()
                    print(self.tabEventCalendar.count)
                }
                
            case .failure(let error):
                print(error)
               // print("In Function getAll Event "+String(self.tab.count))
                
            }
            }.resume()
        
        
        
    }

    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        tabEventCalendar = [Event]()
        getEventByDate(city: "Tunis", state: "TU", country: "Tunisia", DateStart: Int(date.timeIntervalSince1970))
  
      //  print("Nsdate ",NSDate(timeIntervalSince1970: date.timeIntervalSince1970))
     //   print("Date timeinteralSince 1970 ",Date(timeIntervalSince1970: date.timeIntervalSince1970 + 86400))
        
        
        
       
        (cell as? CellView)?.cellSelectionChanged(cellState)
        
        print(tabEventCalendar.count)
      
        
        
    }
    
   
    
    
   /* @IBAction func printSelectedDates() {
        print("\nSelected dates --->")
        for date in calendarView.selectedDates {
            print(formatter.string(from: date))
        }
    }
    */
   /* @IBAction func resize(_ sender: UIButton) {
        calendarView.frame = CGRect(
            x: calendarView.frame.origin.x,
            y: calendarView.frame.origin.y,
            width: calendarView.frame.width,
            height: calendarView.frame.height - 50
        )
    }*/
    
   
    
    @IBAction func next(_ sender: UIButton) {
       // self.calendarView.scrollToNextSegment {
            self.calendarView.visibleDates({ (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            })
       // }
    }
    
    @IBAction func previous(_ sender: UIButton) {
       // self.calendarView.scrollToPreviousSegment {
            self.calendarView.visibleDates({ (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            })
      //  }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
      //  monthLabel.text = monthName + " " + String(year)
    }
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tabEventCalendar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCellCalendar")!
        
        let event: Event
        
            event = tabEventCalendar[indexPath.row]
       
        var lblName :UILabel = cell.viewWithTag(102) as! UILabel
        
        lblName.text = tabEventCalendar[indexPath.row].eventName
        
        let imgImage:UIImageView = cell.viewWithTag(101) as! UIImageView
        
        
        /***/
        let thumbnailURL = URL(string: tabEventCalendar[indexPath.row].thumbUrlLarge)
        let networkService = NetworkService(url: thumbnailURL!)
        networkService.downloadImage { (imageData) in
            let image = UIImage(data: imageData as Data)
            DispatchQueue.main.async(execute: {
                imgImage.image = image
            })
        }
        
         
        
        
        return cell
    }
    

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            performSegue(withIdentifier: "showDetailEventCalendar", sender: tabEventCalendar[indexPath.row])
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : DetailEventCalendar = segue.destination as! DetailEventCalendar
        svc.event = sender as! Event
    }
    
    
    
    
 }
 
 // MARK : JTAppleCalendarDelegate
 extension CalenderEventViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let startDate = formatter.date(from: "2016 10 01")!
        let endDate = formatter.date(from: "2018 12 31")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        (cell as? CellView)?.setupCellBeforeDisplay(cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        (cell as? CellView)?.cellSelectionChanged(cellState)
    }
    
    
    
    // NOTICE: this function is not needed for iOS 10. It will not be called
    func calendar(_ calendar: JTAppleCalendarView, willResetCell cell: JTAppleDayCellView) {
        (cell as? CellView)?.selectedView.isHidden = true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView,
                  sectionHeaderIdentifierFor range: (start: Date, end: Date),
                  belongingTo month: Int) -> String {
        //if month % 2 > 0 {
            return "WhiteSectionHeaderView"
        //}
        //return "PinkSectionHeaderView"
    }
    
    func calendar(_ calendar: JTAppleCalendarView, sectionHeaderSizeFor range: (start: Date, end: Date), belongingTo month: Int) -> CGSize {
       
            return CGSize(width: 200, height: 50)
        
    }
  
    
 }
 
 func delayRunOnMainThread(_ delay: Double, closure: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() +
            Double(Int64(delay * Double(NSEC_PER_SEC))) /
            Double(NSEC_PER_SEC), execute: closure)
 }
