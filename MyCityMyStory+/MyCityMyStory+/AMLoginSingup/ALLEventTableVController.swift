
 //
//  DemoTableViewController.swift
//  TestCollectionView
//
//  Created by Alex K. on 24/05/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import expanding_collection
import Social
import CoreData
import UserNotifications
import FBSDKCoreKit
import FBSDKShareKit

class ALLEventTableVController: ExpandingTableViewController, FBSDKAppInviteDialogDelegate,UNUserNotificationCenterDelegate {
   
    var ResRating : Int = 0

    
    @IBOutlet var shareButtonFacebook: FBSDKShareButton!
    @IBOutlet weak var locationEvent: UILabel!
    var event : Event!
    let rootRef = FIRDatabase.database().reference()
     var evaluations : [Evaluation] = [Evaluation]()
    
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet var invitFriend: UIButton!
    
    fileprivate var scrollOffsetY: CGFloat = 0
    
    /************/
     var isGrantedNotificationAccess:Bool = false
    let center = UNUserNotificationCenter.current()

    /***********/
    
    /**********/
    // Description d'un evenement
    @IBOutlet weak var lblDateStart: UILabel!
    
    @IBOutlet weak var lblDateEnd: UILabel!
    
    @IBOutlet var btnNoParticipant: UIButton!
    @IBOutlet weak var btnParticipant: UIButton!
    @IBOutlet weak var nbrPlace: UILabel!
    
    @IBOutlet weak var nbrPlaceDispo : UILabel!
    
    @IBOutlet weak var txtDescription: UITextView!
    
    /**********/
    @IBOutlet var RatingBarStar: UIButton!
    
    var id : String = ""
    
    @IBAction func didSelectRating(_ sender: Any) {
        
        
        self.makeRatingPopUp()
        
   }
    override func viewDidLoad() {
        super.viewDidLoad()
        //configureNavBar()
        let image1 = UIImage.Asset.BackgroundImage.image
        tableView.backgroundView = UIImageView(image: image1)
        
        /**********/
        let pin = MKPointAnnotation()
        pin.coordinate.latitude = event.venue.latitude
        pin.coordinate.longitude =  event.venue.longitude
        pin.title = event.eventName
        
        map.addAnnotation(pin)
        
        let latDelta:CLLocationDegrees = 0.1
        
        let lonDelta:CLLocationDegrees = 0.1
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(event.venue.latitude, event.venue.longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        map.setRegion(region, animated: true)
        
        
        /*************/
        id = event.Id + "_" + (FIRAuth.auth()?.currentUser?.uid)!
        let nbrParticip = event.nbPlace - event.nbrPlaceDispo
        
        /************/
        
        //Affectation les outlets
        //lblDateStart ,lblDateEnd , nbrPlace ,txtDescription
        //date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        lblDateStart.text = event.startTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        lblDateEnd.text = event.endTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        nbrPlaceDispo.text = "\(event.nbrPlaceDispo!)"
        nbrPlace.text = "\(nbrParticip)"
        txtDescription.text = event.DescEvent
        
        /************/
      
        
        
        
        self.fetchCoreData()
      //  removeNotificaton()
        
        
        /**********/
        

        
        locationEvent.text = event.place
        
        /*******/
        //Configure tableview
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        
    
        
        
        
        /*******/
        /*********/
       // getAllUsersRatingEvent()
        /*********/
        
        
        shareToFacebook()
        
        
         invitFriend.addTarget(self, action: #selector(ALLEventTableVController.inviteButtonTapped), for: UIControlEvents.touchUpInside)
        
         ///////////************************Change button Participate**/
        
        rootRef.child("participations").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(self.event.Id+"_"+(FIRAuth.auth()?.currentUser?.uid)!){
                
                print("true rooms exist")
             
                self.btnParticipant.backgroundColor =  UIColor.gray
                self.btnNoParticipant.backgroundColor = UIColor.green
            }else{
                self.btnParticipant.backgroundColor = UIColor.green
                self.btnNoParticipant.backgroundColor = UIColor.gray
            
            }
            
            
        })
        
        print("----------------")
        self.displayRating(event: self.event.Id)
        print("res Rating = ",self.ResRating)
    }
    
    /*******************************************Rating**************************/
    func makeRatingPopUp () {
        let rateAlert = UIAlertController(title: "Please, Rate Us!", message: "How much do you like this App?", preferredStyle: .alert)
        
        let fiveStarsAction = UIAlertAction(title: "★★★★★", style: .default, handler: {(alert: UIAlertAction!) in self.showCloseAlert( title: "Thank you", message: "We appreciate your opinion.")
            self.makeRating(event: self.event.Id, user: (FIRAuth.auth()?.currentUser?.uid)!, nbrStart: 5)
            
            
        })
        rateAlert.addAction(fiveStarsAction)
        let fourStarsAction = UIAlertAction(title: "★★★★✩", style: .default, handler: {(alert: UIAlertAction!) in self.showCloseAlert( title: "Thank you", message: "We appreciate your opinion.")
            self.makeRating(event: self.event.Id, user: (FIRAuth.auth()?.currentUser?.uid)!, nbrStart: 4)
            
        
        })
        rateAlert.addAction(fourStarsAction)
        let threeStarsAction = UIAlertAction(title: "★★★✩✩", style: .default, handler: {(alert: UIAlertAction!) in self.showCloseAlert( title: "Thank you", message: "We appreciate your opinion.")
            self.makeRating(event: self.event.Id, user: (FIRAuth.auth()?.currentUser?.uid)!, nbrStart: 3)
            
        })
        rateAlert.addAction(threeStarsAction)
        let twoStarsAction = UIAlertAction(title: "★★✩✩✩", style: .default, handler: {(alert: UIAlertAction!) in self.showCloseAlert( title: "Thank you", message: "We appreciate your opinion.")
            self.makeRating(event: self.event.Id, user: (FIRAuth.auth()?.currentUser?.uid)!, nbrStart: 2)
            
        
        })
        rateAlert.addAction(twoStarsAction)
        let oneStarsAction = UIAlertAction(title: "★✩✩✩✩", style: .default, handler: {(alert: UIAlertAction!) in self.showCloseAlert(title: "Thank you", message: "We appreciate your opinion.")
            self.makeRating(event: self.event.Id, user: (FIRAuth.auth()?.currentUser?.uid)!, nbrStart: 1)
            

        })
        rateAlert.addAction(oneStarsAction)
        let notNowAction = UIAlertAction(title: "Not Now", style: .default, handler: nil)
        rateAlert.addAction(notNowAction)
        let noThanksAction = UIAlertAction(title: "Don't Ask Again", style: .default, handler: nil)
        rateAlert.addAction(noThanksAction)
        
        self.present(rateAlert, animated: true, completion: nil)
    }
    func showCloseAlert (title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Close", style: .default, handler: nil)
        alert.addAction(alertAction)
        displayRating(event: self.event.Id)
        self.present(alert, animated: true, completion: nil)
    }

    /**************************************************************************/
    /*******************Make Rating *********/
    func makeRating (event : String , user : String , nbrStart : Int){
        
        let StarRatings = rootRef.child("evaluations").child("\(event)_\(user)")
        
        
        
        let ObjetStarRating : [String : Any] =
            [
                
                "note" : nbrStart ,
                "event_id" : event ,
                "user_id" : user
        ]
        
        
        StarRatings.setValue(ObjetStarRating)
        
        
        
        print("is Updating ")

    }
    /*******************end Rating *********/

    /*******************Display Rating *********/
  
    func displayRating ( event : String )  {


           var nb5 : Int = 0
            var nb4 : Int = 0
            var nb3 : Int = 0
            var nb2 : Int = 0
            var nb1 : Int = 0
        
       
        
            self.rootRef.child("evaluations").observeSingleEvent(of: .value, with: { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    
                    
                    for snap in snapshots {
                        
                        let evaluation : Evaluation = Evaluation(snap: snap)
                        if event == evaluation.eventString {
                            print("********************** nbrStar",evaluation.nbStar)
                             switch evaluation.nbStar
                            {
                            case 1:
                                nb1 = nb1 + 1
                                break
                                
                            case 2:
                                nb2 = nb2 + 1
                                break
                                
                            case 3:
                                nb3 = nb3 + 1
                                break
                            case 4:
                                nb4 = nb4 + 1
                                break
                            case 5:
                                nb5 = nb5 + 1
                                break
                                
                            default:
                                break
                            }
                            
                            
                        }
                        
                    }
                    // res here
                    let sumStar = nb1 + nb2 + nb3 + nb4 + nb5
                    if sumStar != 0 {
                       //  self.ResRating = (nb1 * 1 + nb2 * 2 + nb3 * 3 + nb4 * 4 + nb5 * 5 ) / (sumStar )
                        self.ResRating = (nb1 * 1 + nb2 * 2 + nb3 * 3 + nb4 * 4 + nb5 * 5 ) / (sumStar )
                        print("mother f ",(nb1 * 1 + nb2 * 2 + nb3 * 3 + nb4 * 4 + nb5 * 5 ) / (sumStar ))
                        
                        if self.ResRating == 1 {
                            self.RatingBarStar.setTitle("★✩✩✩✩", for: UIControlState.normal)
                        }else if self.ResRating == 2 {
                             self.RatingBarStar.setTitle("★★✩✩✩", for: UIControlState.normal)

                        }else if self.ResRating == 3 {
                            self.RatingBarStar.setTitle("★★★✩✩", for: UIControlState.normal)

                        }else if self.ResRating == 4 {
                            self.RatingBarStar.setTitle("★★★★✩", for: UIControlState.normal)

                        }else if self.ResRating == 5 {
                            self.RatingBarStar.setTitle("★★★★★", for: UIControlState.normal)

                        }
                        
                    }
                   
                    
                    
                    
                    
                }
            }) { (error) in
                print(error.localizedDescription)
            }
 
 
    }

    
    
    
    /******************* End Rating ******/
    
    
    /**************Invit Friends */
    /**************/
    func inviteButtonTapped()
    {
        print("Invite button tapped")
        
        let inviteDialog:FBSDKAppInviteDialog = FBSDKAppInviteDialog()
        if(inviteDialog.canShow()){
            
            let appLinkUrl:NSURL = NSURL(string: "http://yourwebpage.com")!
            let previewImageUrl:NSURL = NSURL(string: event.urlImage)!
            
            let inviteContent:FBSDKAppInviteContent = FBSDKAppInviteContent()
            inviteContent.appLinkURL = appLinkUrl as URL!
            inviteContent.appInvitePreviewImageURL = previewImageUrl as URL!
            
            inviteDialog.content = inviteContent
            inviteDialog.delegate = self
            inviteDialog.show()
        }
    }
    
    /**
     Sent to the delegate when the app invite completes without error.
     - Parameter appInviteDialog: The FBSDKAppInviteDialog that completed.
     - Parameter results: The results from the dialog.  This may be nil or empty.
     */
    public func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
        let resultObject = NSDictionary(dictionary: results)
        
        if let didCancel = resultObject.value(forKey: "completionGesture")
        {
            if (didCancel as AnyObject).caseInsensitiveCompare("Cancel") == ComparisonResult.orderedSame
            {
                print("User Canceled invitation dialog")
            }
        }
        
        
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("Error tool place in appInviteDialog \(error)")
    }
    
    
    @IBAction func btnParticipateNoAction(_ sender: Any) {
        //POST
        let post : [String : Any]  = [
            "title": event.eventName,
            "urlImage": event.urlImage,
            "category": event.categories,
            "place" : event.place,
            "description" : event.DescEvent,
            "latitudeEvent" : event.venue.latitude ,
            "longitudeEvent" : event.venue.longitude ,
            "nbPlaces": Int(event.nbPlace),
            "nbAvailablePlaces": Int(event.nbrPlaceDispo)+1,
            "user_id" : event.user_id,
            "startDate" : event.startTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma"),
            "endDate" : event.endTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma"),
            
            ]
        
        rootRef.child("events").child(event.Id).setValue(post)
        
        
        rootRef.child("participations").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if (snapshot.hasChild(self.id)) {
               // self.btnParticipant.setTitle("Participate", for:UIControlState.normal)
                let participateItemRef = self.rootRef.child("participations").child(self.id)
                print (self.id)
               participateItemRef.removeValue()
                 
                 
                 /********************/
                 // self.desabonner()
                 self.AnnulerCoreData()
                 self.removeNotificaton()
                 /********************/
                self.btnParticipant.backgroundColor = UIColor.green
                self.btnNoParticipant.backgroundColor = UIColor.gray
                
            }
        })
        

    }
    /*******************************/
    
    @IBAction func btnParticipateAction(_ sender: Any) {
        
        //POST
        let post : [String : Any]  = [
            "title": event.eventName,
            "urlImage": event.urlImage,
            "category": event.categories,
            "place" : event.place,
            "description" : event.DescEvent,
            "latitudeEvent" : event.venue.latitude ,
            "longitudeEvent" : event.venue.longitude ,
            "nbPlaces": Int(event.nbPlace),
            "nbAvailablePlaces": Int(event.nbrPlaceDispo)-1,
            "user_id" : event.user_id,
            "startDate" : event.startTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma"),
            "endDate" : event.endTime.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma"),
            
        ]

        rootRef.child("events").child(event.Id).setValue(post)
        
        
        rootRef.child("participations").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if (snapshot.hasChild(self.id)) {
                self.btnParticipant.setTitle("Participate", for:UIControlState.normal)
                let participateItemRef = self.rootRef.child("participations").child(self.id)
                print (self.id)
                /*participateItemRef.removeValue()
                
                
                /********************/
               // self.desabonner()
                self.AnnulerCoreData()
                self.removeNotificaton()
                /********************/
                 */
                
            }
                
            else {
                
                self.btnParticipant.backgroundColor = UIColor.gray
                self.btnNoParticipant.backgroundColor = UIColor.green
          
                if ( self.event.nbrPlaceDispo == 0 )   {
                    self.btnParticipant.isHidden = true
                    self.nbrPlaceDispo.isHidden = false
                }
                else {
                    self.btnParticipant.isHidden = false
                    self.nbrPlaceDispo.isHidden = true
                    
                    
                   
                    
                    let participateItem = Participation (event_id: self.event.Id, user_id: (FIRAuth.auth()?.currentUser?.uid)!
                        , id: self.id)        // 3
                    //let nbrPlaceDispo = event.nbPlace
                    let participateItemRef = self.rootRef.child("participations").child(self.id)
                    
                    participateItemRef.setValue(participateItem.toAnyObject())
                    
                   /*******************/
                  //  self.sabonner()
                    self.ParticiperCoreData()
                    self.sendNotification()
                  /*******************/
                    
                    
                }
         
                
            }
        })

        
    }
  /*  func  getAllUsersRatingEvent(){
        
        self.evaluations = [Evaluation]()
        let rootref = FIRDatabase.database().reference()
        rootref.child("evaluations").observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    
                    let ev = snap.childSnapshot(forPath: "event").value as! String
                    if ev == self.event.Id {
                        self.evaluations.append(Evaluation(snap: snap))
                    }
                   
                    
                    
                }
                print(" nombre d evaluation ",self.evaluations.count)
                
                //   self.tableView.reloadData()
                
            }
        }) { (error) in
            print("erreur"+error.localizedDescription)
        }
        
        
        
    }*/

    /***************SendNotification + add in core Date***********************/
    
    func ParticiperCoreData() {
        // create an instance of our managedObjectContext
        let moc = DataController().managedObjectContext
        // we set up our entity by selecting the entity and context that we're targeting
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Participer", into: moc) as!
            Participer
        // add our data
        entity.setValue(event.Id, forKey: "event_id")
        entity.setValue("1", forKey: "user_id")
        /***********************/
        
        let calendar = Calendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: self.event.startTime)
        
        
        let dateFire: NSDateComponents = NSDateComponents()
        
        
        dateFire.year = dateComponents.year!
        dateFire.month = dateComponents.month!
        dateFire.day = dateComponents.day!
        dateFire.hour = dateComponents.hour! + 1
        dateFire.minute = dateComponents.minute!
        dateFire.timeZone = NSTimeZone.default
        
        let date: NSDate = calendar.date(from: dateFire as DateComponents)! as NSDate
        
        
        
        /***********************/
        
        entity.setValue(date, forKey: "date")
        // we save our entity
        do {
            try moc.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func AnnulerCoreData() {
        
        // create an instance of our managedObjectContext
        let moc = DataController().managedObjectContext
        // we set up our entity by selecting the entity and context that we're targeting
        
        let fetchRequest: NSFetchRequest<Participer> = Participer.fetchRequest()
        print("event_id==\(event.Id!) AND user_id==1")
        fetchRequest.predicate = NSPredicate.init(format: "event_id='\(event.Id!)' AND user_id='1'")
        if let result = try? moc.fetch(fetchRequest) {
            for object in result {
                moc.delete(object)
            }
        }
        
        // we save our entity
        do {
            try moc.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func fetchCoreData() {
        
        // create an instance of our managedObjectContext
        let moc = DataController().managedObjectContext
        // we set up our entity by selecting the entity and context that we're targeting
        
        let fetchRequest: NSFetchRequest<Participer> = Participer.fetchRequest()
        if let result = try? moc.fetch(fetchRequest) {
            for object in result {
                let  b = object
                print("event \(b.event_id) user : \(b.user_id) date \(b.date)")
            }
        }
        
        // we save our entity
        do {
            try moc.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    
    func sendNotification(){

        
        print("send Notification before ")
        fetchCoreData()
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: self.event.startTime)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)

        
       
        let content = UNMutableNotificationContent()
        content.title = "Pop Quiz!"
        content.subtitle = "Let's see how smart you are!"
        content.body = "How many countries are there in Africa?"
        content.badge = 1
        content.categoryIdentifier = "quizCategory"
        
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        let requestIdentifier = "africaQuiz"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            // handle error
        })
    }
    
    func removeNotificaton(){
        let scheduledNotifications: [UILocalNotification]? = UIApplication.shared.scheduledLocalNotifications
        //   guard scheduledNotifications != nil else {return} // Nothing to remove, so return
        print("remove")
        for notification in scheduledNotifications! { // loop through notifications...
            /* if (notification.userInfo!["UUID"] as! Int == self.id) { // ...and cancel the notification that corresponds to this TodoItem instance (matched
             UIApplication.shared.cancelLocalNotification(notification) // there should be a maximum of one match on UUID
             
             
             
             break
             
             
             }
             */
            print("mlok",notification.fireDate,notification.userInfo)
        }
        
        
    }

    
    /********************************************/
}
// MARK: Helpers

extension ALLEventTableVController {
    
    fileprivate func configureNavBar() {
        navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        navigationItem.rightBarButtonItem?.image = navigationItem.rightBarButtonItem?.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
}

// MARK: Actions

extension ALLEventTableVController {
    
    @IBAction func backButtonHandler(_ sender: AnyObject) {
        // buttonAnimation
        let viewControllers: [DemoViewController?] = navigationController?.viewControllers.map { $0 as? DemoViewController } ?? []
        
        
        
        for viewController in viewControllers {
            if let rightButton = viewController?.navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(false)
            }
        }
        popTransitionAnimation()
    }
}

// MARK: UIScrollViewDelegate

extension ALLEventTableVController {
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -25 {
            // buttonAnimation
            let viewControllers: [DemoViewController?] = navigationController?.viewControllers.map { $0 as? DemoViewController } ?? []
            
            for viewController in viewControllers {
                if let rightButton = viewController?.navigationItem.rightBarButtonItem as? AnimatingBarButton {
                    rightButton.animationSelected(false)
                }
            }
            popTransitionAnimation()
        }
        scrollOffsetY = scrollView.contentOffset.y
    }
}

// TABLEAU : Implementer tableau par les user qui on evaluer l'evenement
extension ALLEventTableVController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return evaluations.count + 2
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (cell.reuseIdentifier == "myCellALLEvent"){
            if (indexPath.row > 1  ){
                let lblUserName:UILabel = cell.viewWithTag(102) as! UILabel
                lblUserName.text =  "asdsd"
                //evaluations[indexPath.row-2].userr.Firstname
                //  let imageStar : UIImageView = cell.viewWithTag(103) as! UIImageView
                // let nb = "\(evaluations[indexPath.row-2].nbStar)"
                //  imageStar.image = UIImage(named: "stars\(nb)")
            }
            
            
        }
        
    }
    
    
    func shareToFacebook(){
        let urlImage = NSURL(string: event.urlImage)
        
        
        
        let content = FBSDKShareLinkContent()
        content.contentTitle = event.eventName
        content.imageURL = urlImage as URL!
        
        
        shareButtonFacebook.shareContent = content
        
        
        

    }
   
    
 
  
    
}
