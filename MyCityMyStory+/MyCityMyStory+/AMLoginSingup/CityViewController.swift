 
     //
    //  CountryViewController.swift
    //  MyCityMyStory+
    //
    //  Created by ESPRIT on 25/12/2016.
    //  Copyright © 2016 amirs.eu. All rights reserved.
    //
    
    import UIKit
    
    class CityViewController: UIViewController,UISearchBarDelegate ,UITableViewDelegate,UITableViewDataSource {
        
        var countryArray : [String]  = [String]()
        let searchController = UISearchController(searchResultsController: nil)
        var tabEventNearMeSearch : [String] = [String] ()

        @IBOutlet weak var tableView: UITableView!
        override func viewDidLoad() {
            super.viewDidLoad()
            if  UserDetailViewController.isSelectedCountry == true || RegisterDetailUserController.isSelectedCountry  {
                countryArray = ReaderCountry.arrayCountys[UserDetailViewController.countrySelectedIndex].cities!
            }
            // Do any additional setup after loading the view.
            tableView.delegate = self
            tableView.dataSource = self
            //Setup search bar
            searchController.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true
            //Set delegate
            searchController.searchResultsUpdater = self
            tableView.tableHeaderView = searchController.searchBar
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        func filterContentForSearchText(searchText: String, scope: String = "All") {
            tabEventNearMeSearch = countryArray.filter { candy in
                return (candy.lowercased().contains(searchText.lowercased()))
            }
            
            self.tableView.reloadData()
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if searchController.isActive && searchController.searchBar.text != "" {
                return tabEventNearMeSearch.count
            }
            
            return countryArray.count    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
          
            
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellCity")!
            if searchController.isActive && searchController.searchBar.text != "" {
                cell.textLabel?.text = tabEventNearMeSearch[indexPath.row]
            } else {
                cell.textLabel?.text = countryArray[indexPath.row]
            }
            
            
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            
            if  UserDetailViewController.isSelectedCountry == true || RegisterDetailUserController.isSelectedCountry == true  {
                
            
            if searchController.isActive && searchController.searchBar.text != "" {
                var indexSelectedSeart : Int = 0
                for i in 0...(countryArray.count - 1) {
                    if  countryArray[i] ==  tabEventNearMeSearch[indexPath.row] {
                        indexSelectedSeart = i
                        break
                    }
                }
                UserDetailViewController.citySelected =  countryArray[indexSelectedSeart]
                
                RegisterDetailUserController.citySelected =  countryArray[indexSelectedSeart]
                searchController.isActive = false
                self.dismiss(animated: true, completion : nil)
                
            }else{
                UserDetailViewController.citySelected =  countryArray[indexPath.row]
                
                RegisterDetailUserController.citySelected =  countryArray[indexPath.row]
                self.dismiss(animated: true, completion : nil)
                
        }
            }
    }
            
   
        
        
        
 }
 extension CityViewController : UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
    
 }
 
