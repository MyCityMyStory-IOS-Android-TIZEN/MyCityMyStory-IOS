//
//  Evaluation.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/1/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Firebase

class Evaluation: NSObject {
    
    var Id : String = ""
    var event : Event!
    var nbStar : Int = 0
    var userr  : EntityUser!
    
    var userString = ""
    var eventString  = ""
    let rootref = FIRDatabase.database().reference()

    init( snap :FIRDataSnapshot) {
        super.init()
       
          Id = "\(snap.key)"
          userString = snap.childSnapshot(forPath: "user_id").value as! String
          eventString = snap.childSnapshot(forPath: "event_id").value as! String
          nbStar = snap.childSnapshot(forPath: "note").value as! Int
        
        
    }
    
    
}
