//
//  DetailAllEventViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Syrine Dridi on 11/11/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import FirebaseAuth
import Alamofire
import  FirebaseDatabase
class DetailAllEventViewController : UIViewController {
    var participate : Participation!
    var  event : Event!
    var tempp : Double!
    var mainTemp : [String: AnyObject]!
    let rootRef = FIRDatabase.database().reference()
    let ref: FIRDatabaseReference? = nil
    @IBOutlet weak var imageEvent: UIImageView!
    
    
    @IBOutlet weak var imageWeatherView: UIImageView!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var btnParticipate: UIButton!
    
    @IBOutlet weak var DateTimeStart: UILabel!
    
    @IBOutlet weak var DateTimeEnd: UILabel!
    
    @IBOutlet weak var lblTemperature: UILabel!
    
    var user_id = ""
    var event_id = ""
    var name = ""
    var desc = ""
    var nbPlace = 0
    var nbPlacedispo = 0
    var eventuser = ""
    var cat = ""
    var place = ""
    var urlImage = ""
    
    
    
    var id = ""
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        user_id = (FIRAuth.auth()?.currentUser?.uid)!
        event_id = event.Id
        name = event.eventName
        desc = event.DescEvent
        nbPlace = event.nbPlace
        nbPlacedispo = event.nbrPlaceDispo
        eventuser = event.user_id
        cat = event.categories
        urlImage=event.urlImage
        id = event_id+user_id
        /***********Date Time start & end***********/
        DateTimeStart.text=event.categories
        DateTimeEnd.text=event.place
        /********************************************/
        
        /*********Location************/
        btnLocation.setTitle(event.place, for:UIControlState.normal)
        /*****************************/
       
        rootRef.child("participations").observeSingleEvent(of: .value, with: { (snapshot) in
            print (snapshot.value)
            if (snapshot.hasChild(self.id)) {
            self.btnParticipate.setTitle("Annuler", for:UIControlState.normal)
            print("annuleeeeer")
        }
      
        //  lblTemperature.text = event.temperature as! String
        /**********/
        
        /***********/
           
    })
    }

    
    override func viewWillAppear(_ animated: Bool) {
        do{
            getTempBy(Latitude: event.venue.latitude,longitude: event.venue.longitude)
            
        }
        
    }
    
  
    
    @IBAction func btnParticipateAction(_ sender: AnyObject) {

        
        let post : [String : Any]  = [
            "title": name,
            "urlImage": urlImage,
            "category": cat,
            "place" : place,
            "description" : desc,
            "latitudeEvent" : 9.08987 ,
            "longitudeEvent" : 9.08987 ,
            "nbPlaces": Int(nbPlace),
            "nbAvailablePlaces": Int(nbPlacedispo)-1,
            "user_id" : eventuser
            
        ]
        self.rootRef.child("events").child(event_id).setValue(post)
      
     
        rootRef.child("participations").observeSingleEvent(of: .value, with: { (snapshot) in
         
            if (snapshot.hasChild(self.id)) {
             self.btnParticipate.setTitle("Participate", for:UIControlState.normal)
            let participateItemRef = self.rootRef.child("participations").child(self.id)
            print (self.id)
            participateItemRef.removeValue()
            
        }
        
        else {
        self.btnParticipate.setTitle("Annuler", for:UIControlState.normal)
        let participateItem = Participation (event_id: self.event_id, user_id: self.user_id, id: self.id)        // 3
                //let nbrPlaceDispo = event.nbPlace
        let participateItemRef = self.rootRef.child("participations").child(self.id)
            print (self.id)
        participateItemRef.setValue(participateItem.toAnyObject())
        
            }
        })
    }
    
    @IBAction func btnLogOutAction(_ sender: AnyObject) {
     try! FIRAuth.auth()?.signOut()
        
        
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as UIViewController!
        self.present(nextViewController!, animated:true, completion:nil)
        
        
    }
    /*********************/
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getIconWeather(){
        //http://openweathermap.org/img/w/10d.png
        
    }
    
    func getTempBy(Latitude lat : Double , longitude lon : Double) {
        
        
        
        
        /***Temp**/
        //http://api.openweathermap.org/data/2.5/find?lat=36.812519073486&lon=10.170880317688&cnt=1&appid=949b43b9ef9cbc4e772477f7cba8fd5e
        /****ICON **/
        //https://openweathermap.org/weather-conditions
        /******/
        
        Alamofire.request("http://api.openweathermap.org/data/2.5/find?lat=\(event.venue.latitude)&lon=\(event.venue.longitude)&cnt=1&appid=949b43b9ef9cbc4e772477f7cba8fd5e", method: .get ).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value{
                    let jsonResult:Dictionary = json  as! [String: Any]
                    let list : NSArray = (jsonResult["list"] as! NSArray)
                    let mainWeather : Dictionary = list[0] as! [String: Any]
                    
                    
                    
                    self.mainTemp   = mainWeather["main"] as! [String: AnyObject]
                    //  print("Ahmed \(self.mainTemp["temp"]! as! Double)")
                    
                    self.lblTemperature.text = "\(self.mainTemp["temp"]! as! Double)"
                    
                    let listWeather : NSArray = (mainWeather["weather"] as! NSArray)
                    let objWeather : Dictionary = listWeather[0] as! [String: Any]
                    
                    /*********************/
                    /********IconWeather********/
                    let iconURL = URL(string: "http://openweathermap.org/img/w/\(objWeather["icon"]!).png"  )
                    let networkService = NetworkService(url: iconURL!)
                    networkService.downloadImage { (imageData) in
                        let image = UIImage(data: imageData as Data)
                        DispatchQueue.main.async(execute: {
                            self.imageWeatherView.image = image
                        })
                    }
                    
                    /**************************/
                    
                    /*********************/
                    
                    
                }
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let svc : MapEventViewController = segue.destination as! MapEventViewController
      svc.event = event
   }
    
    
}
