//
//  SelectSeachEventViewController.swift
//  MyCityMyStory+
//
//  Created by Chebbi on 12/4/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit

class SelectSeachEventViewController: MIPivotPage {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     @IBAction func TodayAction(_ sender: Any) {
     
          performSegue(withIdentifier: "segueSearchEvent", sender: "Today")
    }
    
    @IBAction func TomorrowAction(_ sender: Any) {
              performSegue(withIdentifier: "segueSearchEvent", sender: "Tomorrow")
    }
 
  
    
    
    @IBAction func Ongoing(_ sender: Any) {
        
        performSegue(withIdentifier: "segueSearchEvent", sender: "Ongoing")
        
    }
    
   
    
    @IBAction func FindEventFromTo(_ sender: Any) {
        
           performSegue(withIdentifier: "segueSearchEvent", sender: "FindEvent")
    }
    
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : SearchEventAPIViewController = segue.destination as! SearchEventAPIViewController
        svc.TypeSearch = sender as! String
    }
   
   
    
}
