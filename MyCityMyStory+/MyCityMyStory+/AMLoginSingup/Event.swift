//
//  Event.swift
//  MyCityMyStory
//
//  Created by Chebbi on 11/2/16.
//  Copyright © 2016 Chebbi. All rights reserved.
//

import UIKit

import Foundation
import Firebase

class Event: NSObject {

    
    var Id : String!
    
    /* Start these objects will recover by AllEvents API */
    var  eventIdFb  : String!
        var  eventIdAPI  : Int64!
    var  eventName : String!
    var   thumbUrl:String!
    var   thumbUrlLarge:String!
    var   bannerUrl:String!
    var   startTime:Date!
    var   startTimeDisplay:String!
    
    var   endTime:Date!
    var   endTimeDisplay:String!
    
    var   location:String!
    var   label :String!
    var   eventUrl:String!
    var   shareUrl:String!
     var   score:Double!
    var categories:String!
    var tags:String!

    var  venue :Venue!
    
    var temperature : Double!
    
    /* end these objects will recover by AllEvents API */
    
    /* Start these objects will recover by FB API */
   // var  descrip:String!
   // var   endTime:NSDate!
    /* End these objects will recover by FB API */
    
    var place : String!
    var nbPlace : Int!
    var nbrPlaceDispo : Int!
    var DescEvent : String!
    var user_id : String!
    var urlImage:String!
    
    
    override init() {
        
        
    }
    
    init(eventName: String!, categories: String!, placee: String! ,nbPlace :Int!,DescEvent :String!,nbrPlaceDispo : Int) {
        self.eventName = eventName
        //  self.id = id
        self.categories = categories
        self.nbPlace = nbPlace
        // self.addedByUser = addedByUser
        self.place = placee
        self.DescEvent = DescEvent
        self.nbrPlaceDispo = nbrPlaceDispo
        
        
        
    }
    //startDate,endDate
    //
    //  startTime = dateFormatter.date(from: dic["date_debut"]as! String)
    
    init( snap :FIRDataSnapshot) {
    
            Id = "\(snap.key)"
            eventName = snap.childSnapshot(forPath: "title").value as! String
            DescEvent = snap.childSnapshot(forPath: "description").value as! String
            place = snap.childSnapshot(forPath: "place").value as! String
            nbPlace = snap.childSnapshot(forPath: "nbPlaces").value as! Int
            categories = snap.childSnapshot(forPath: "category").value as! String
            let   latitude  = snap.childSnapshot(forPath: "latitudeEvent").value as! Double
            let   longitude = snap.childSnapshot(forPath: "longitudeEvent").value as! Double
            venue = Venue(Lat :latitude ,Long : longitude)
        
            place = snap.childSnapshot(forPath: "place").value as! String
            nbrPlaceDispo = snap.childSnapshot(forPath: "nbAvailablePlaces").value as! Int
            user_id = snap.childSnapshot(forPath: "user_id").value as! String
            urlImage = snap.childSnapshot(forPath: "urlImage").value as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d' 'MMMM' 'yyyy', 'h':'mma"
        startTime = dateFormatter.date(from:  snap.childSnapshot(forPath: "startDate").value as! String)
        endTime = dateFormatter.date(from:  snap.childSnapshot(forPath: "endDate").value as! String)

        
    }
    
    
    
    func toAnyObject() -> Any {
        return [
            "title": ""+eventName,
            "urlImage": "",
            "category": ""+categories ,
            "place": ""+place,
            "description" : ""+DescEvent,
            "latitudeEvent" : 9.08987 ,
            "longitudeEvent" : 9.08987 ,
            "nbPlaces": nbPlace,
            "nbAvailablePlaces": nbrPlaceDispo,
            "user_id" : FIRAuth.auth()?.currentUser?.uid
            
        ]
    }
    
 
      init(dic : [String:Any]) {
        /* Start these objects will recover by AllEvents API */
        
           eventName  = dic["eventname"] as! String
       eventIdAPI  = Int64 (dic["owner_id"] as! String )
         thumbUrlLarge = dic["thumb_url_large"]as! String
           startTimeDisplay = dic["start_time_display"]as! String
        location = dic["location"]as! String
         endTimeDisplay = dic["end_time_display"]as! String
        venue  =  Venue(dic: dic["venue"] as! [String:Any])
        startTime =  Date(timeIntervalSince1970: dic["start_time"] as! Double)
       print("From Entity Event : ",startTime)
        shareUrl = dic["share_url"]as! String
            eventUrl = dic["event_url"]as! String
        label = dic["label"]as! String

     eventIdFb  = dic["event_id"] as! String
   /*     thumbUrl = dic["thumb_url"] as! String
       
        startTime = dic["start_time"] as! NSDate
     
        endTime = dic["end_time"] as! NSDate
       
        
        venue  =  Venue(dic: dic["venue"] as! [String:Any])
        label  = dic["label"]as! String
        eventUrl = dic["event_url"]as! String
        bannerUrl = dic["banner_url"]as! String
        score = dic["banner_url"]as! Double
        categories = dic["categories"]as! String
        tags = dic["categories"]as! String
         
*/
        
        
        /* end these objects will recover by AllEvents API */
        
        
    }
    
      

}
