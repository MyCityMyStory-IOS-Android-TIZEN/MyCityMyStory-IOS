//
//  MyEventViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Chebbi on 11/18/16.
//  Copyright © 2016 Developers Academy. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
class MyEventViewController:UITableViewController {
    let rootref = FIRDatabase.database().reference()
    //  @IBOutlet weak var menuButton: UIBarButtonItem!
    
    
    var tab : [Event] = [Event] ()
    var temp :Temperature!
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        // Make the row height dynamic
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // displayWalkthroughs()
        
        // burger side bar menu
    }
    override func viewWillAppear(_ animated: Bool) {
        tab   = [Event]()
        getAllEvents()
        
        
    }
    
    func getAllEvents()   {
        
        rootref.child("events").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    let ee : Event = Event(snap: snap)
                    if ee.user_id == "0v8dse3jQ6YWTOIkt3smtROvfPQ2"{
                    self.tab.append(ee)
                    }
                  
                }
                self.tableView.reloadData()
                
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tab.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCellMyEvent")!
        
        
        let lblUserName:UILabel = cell.viewWithTag(101) as! UILabel
        lblUserName.text = tab[indexPath.row].eventName
        let lblDateStart:UILabel = cell.viewWithTag(102) as! UILabel
        lblDateStart.text = tab[indexPath.row].place
        /*
         /***/
         let imgProfile:UIImageView = cell.viewWithTag(103) as! UIImageView
         let thumbnailURL = URL(string: tab[indexPath.row].thumbUrlLarge)
         
         /***/
         
         let networkService = NetworkService(url: thumbnailURL!)
         networkService.downloadImage { (imageData) in
         let image = UIImage(data: imageData as Data)
         DispatchQueue.main.async(execute: {
         imgProfile.image = image
         })
         }
         */
        
        
        
        
        return cell
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MyEventDetailSegueAllEvents", sender: tab[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc : DetailAllEventViewController = segue.destination as! DetailAllEventViewController
        svc.event = sender as! Event
    }
    
    
}

