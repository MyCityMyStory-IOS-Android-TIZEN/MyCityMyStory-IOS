//
//  ListCatViewController.swift
//  MyCityMyStoryVersion1.1
//
//  Created by Chebbi on 01/12/2016.
//  Copyright © 2016 Developers Academy. All rights reserved.
//
import FirebaseAuth
import Firebase
import  FirebaseDatabase
import UIKit

class ListCatViewController: UIViewController{
    
    let rootRef = FIRDatabase.database().reference()
    
    let identifier = "CellIdentifier"
    let headerViewIdentifier = "HeaderView"
   
    var interst: Set = [""]
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    let dataSource = DataSource()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
     collectionView?.allowsMultipleSelection = true
        
       // navigationItem.leftBarButtonItem = editButtonItem
        toolBar.isHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
       
  
        

 
        
        if let indexPath = getIndexPathForSelectedCell() {
            highlightCell(indexPath, flag: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- prepareForSegue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // retrieve selected cell & fruit
        
        if let indexPath = getIndexPathForSelectedCell() {
        
  //          let fruit = dataSource.fruits(indexPath.section)[indexPath.row]
            
       //     let detailViewController = segue.destination as! DetailViewController
        //    detailViewController.fruit = fruit
        }
    }
    
    // MARK:- Should Perform Segue
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !isEditing
    }
    
    // MARK:- Selected Cell IndexPath
    
    func getIndexPathForSelectedCell() -> IndexPath? {
        
        var indexPath:IndexPath?
        
        if collectionView.indexPathsForSelectedItems!.count > 0 {
            indexPath = collectionView.indexPathsForSelectedItems![0]
        }
        return indexPath
    }
    
    // MARK:- Highlight
    
    func highlightCell(_ indexPath : IndexPath, flag: Bool) {
        
        let cell = collectionView.cellForItem(at: indexPath)
        
        if flag {
            cell?.contentView.backgroundColor = UIColor.magenta
        } else {
            cell?.contentView.backgroundColor = nil
        }
    }
    
    // MARK:- Editing
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        collectionView?.allowsMultipleSelection = editing
        toolBar.isHidden = !editing
    }
    
    @IBAction func dissmiss(_ sender: AnyObject) {
        
         self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- Add Cell
    
    @IBAction func addNewItem(_ sender: AnyObject) {
 interst.remove("")
 print(interst)
        let userId = FIRAuth.auth()?.currentUser?.uid
        print(userId as Any)
        let interest = Array(interst)
        let interstItem = Interest (interest : interest)
        let interstItemRef = rootRef.child("interest").child(userId!)
        interstItemRef.setValue(interstItem.toAnyObject())
        
        
        
     }
    
    
    @IBAction func deleteCells(_ sender: AnyObject) {
        
        var deletedFruits:[Fruit] = []
        
        let indexpaths = collectionView?.indexPathsForSelectedItems
        
        if let indexpaths = indexpaths {
            
            for item  in indexpaths {
                collectionView?.deselectItem(at: (item), animated: true)
                // fruits for section
                let sectionfruits = dataSource.fruitsInGroup(item.section)
                deletedFruits.append(sectionfruits[item.row])
            }
            
            dataSource.deleteItems(deletedFruits)
            
            collectionView?.deleteItems(at: indexpaths)
            
          }
    }
}

// MARK:- UICollectionView DataSource

extension ListCatViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numbeOfRowsInEachGroup(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,for:indexPath) as! FruitCell
        
        let fruits: [Fruit] = dataSource.fruitsInGroup(indexPath.section)
        let fruit = fruits[indexPath.row]
        let name = fruit.name!
        cell.imageView.image = UIImage(named: name)
        cell.caption.text = name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView: FruitsHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerViewIdentifier, for: indexPath) as! FruitsHeaderView
        
        //headerView.sectionLabel.text = dataSource.gettGroupLabelAtIndex(indexPath.section)
        
        return headerView
    }
}

// MARK:- UICollectionViewDelegate Methods

extension ListCatViewController : UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       

        
        let fruits: [Fruit] = dataSource.fruitsInGroup(indexPath.section)
        let fruit = fruits[indexPath.row]
        
        let name = fruit.name!
        
        
        let cell = collectionView.cellForItem(at: indexPath)
        if (cell?.isSelected == true ){
            cell?.backgroundColor = UIColor.yellow
            interst.insert(name)
            print(name,"name")
        }
      
     //   highlightCell(indexPath, flag: true)
        print(interst)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //highlightCell(indexPath, flag: false)
        let fruits: [Fruit] = dataSource.fruitsInGroup(indexPath.section)
        let fruit = fruits[indexPath.row]
        let name = fruit.name!
        let cell = collectionView.cellForItem(at: indexPath)
        if (cell?.isSelected == false ){
            cell?.backgroundColor = UIColor.white
            interst.remove(name)
        }
        print(interst)
   
    }
}

extension ListCatViewController: UICollectionViewDelegateFlowLayout {
    // MARK:- UICollectioViewDelegateFlowLayout methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        // http://stackoverflow.com/questions/28872001/uicollectionview-cell-spacing-based-on-device-sceen-size
        
        let length = (UIScreen.main.bounds.width-15)/2
        return CGSize(width: length,height: length);
    }
}


