//
//  Participer+CoreDataProperties.swift
//  
//
//  Created by Chebbi on 12/4/16.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Participer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Participer> {
        return NSFetchRequest<Participer>(entityName: "Participer");
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var event_id: String?
    @NSManaged public var user_id: String?

}
